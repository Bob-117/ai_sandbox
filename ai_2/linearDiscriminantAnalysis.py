import matplotlib.pyplot as plt
import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis


leaf = np.loadtxt('data/leaf/leaf.csv', delimiter=',')
print(leaf[:2,:])

lda = LinearDiscriminantAnalysis()
lda.fit(leaf[:,2:],leaf[:,0])

# On transforme le résultat de notre analyse discriminante pour pouvoir l'afficher avec Matplotlib
leafl = lda.transform(leaf[:,2:])
fig = plt.figure()

# On transforme la projection en 3D
ax = fig.add_subplot(111, projection='3d')

ax.scatter(leafl[:,0],leafl[:,1],leafl[:,2],c=leaf[:,0])
plt.title('Analyse de feuilles d\'arbres en fonction de leur espèce, de leur numéro de spécimem et de leur excentricité')
plt.show()

from sklearn.model_selection import train_test_split
train, test = train_test_split(leaf, test_size = 0.2)
lda.fit(train[:,2:],train[:,0])
lda.score(test[:,2:],test[:,0])
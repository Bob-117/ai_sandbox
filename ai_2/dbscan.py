from sklearn.datasets import make_blobs
from sklearn.cluster import DBSCAN
import numpy as np
import matplotlib.pyplot as plt

num_samples = 2400
cluster_centers = [(3,3), (6,9), (10,5), (12,7)]
num_classes = len(cluster_centers)
epsilon = 0.4
min_samples = 14

X, y = make_blobs(n_samples=num_samples, centers=cluster_centers, n_features=num_classes, center_box=(0, 1), cluster_std = 0.5)

db = DBSCAN(eps=epsilon, min_samples=min_samples).fit(X)
labels = db.labels_

no_clusters = len(np.unique(labels) )
no_noise = np.sum(np.array(labels) == -1, axis=0)

print('Estimated no. of clusters: %d' % no_clusters)
print('Estimated no. of noise points: %d' % no_noise)

colors = list(map(lambda x: '#3b4cc0' if x == 1 else ('#00ff00' if x == 2 else ('#b40426' if x == 0 else '#000000')), labels))

plt.figure(1)
plt.scatter(X[:,0], X[:,1], c=colors, marker=".", picker=True)
plt.title('Présentation de DBSCAN')
plt.xlabel('Axis X[0]')
plt.ylabel('Axis X[1]')
plt.show()
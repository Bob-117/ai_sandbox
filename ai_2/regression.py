import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import linear_model

df = pd.read_excel('data/fruitDataset.xlsx')
print(df.head())

fruit_cible = dict(zip(df.etiquette_fruit.unique(), df.nom.unique()))

print(fruit_cible)

# Valeurs caractéristiques et valeur cible
x = df [['poids', 'largeur', 'hauteur']]
y = df['etiquette_fruit']

# Fractionnement du dataset, instanciation de la regression et entrainement
x_train, x_test, y_train, y_test = train_test_split(x, y)
modele_regLog = linear_model.LogisticRegression(solver = 'liblinear')
modele_regLog.fit(x_train.values, y_train)

# Obtention de la précision du modèle
precision = modele_regLog.score(x_test.values, y_test)
print(precision * 100)

# Prédiction pour une pomme
prediction_fruit = modele_regLog.predict([[180, 8.0, 6.8]])
print('Résultat de la prédiction : ' + fruit_cible[prediction_fruit[0]])

# Prédiction pour une mandarine
prediction_fruit = modele_regLog.predict([[20, 4.3, 5.5]])
print('Résultat de la prédiction : ' + fruit_cible[prediction_fruit[0]])

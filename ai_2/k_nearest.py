from matplotlib import pyplot as plt
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier

iris = load_iris()
x = iris.data[:, 0]
y = iris.data[:, 1]
type = iris.target

plt.figure(figsize=(8, 5))
plt.scatter(x, y, c=iris.target)
formatter = plt.FuncFormatter(lambda i, *args: iris.target_names[int(i)])
plt.colorbar(ticks=[0, 1, 2], format=formatter)
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.title('k plus proche voisins')

length = 5
width = 4
plt.scatter(length, width, color='k', marker="x")
k = 3
d = list(zip(x, y)) # Regroupement en couples de coordonnées (x, y)
model = KNeighborsClassifier(n_neighbors = k)
model.fit(d, type)
prediction = model.predict([[length, width]])
plt.text(1.8, 1.6, "Résultat : " + iris.target_names[prediction[0]], fontsize=12)
print("Résultat " + iris.target_names[prediction[0]])

plt.show()
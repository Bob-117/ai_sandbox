# AI DROPBOX


| Variable              |
|-----------------------|
| Quantitative continue |
| Quantitative discrete |
| Qualitative nominale  |
| Qualitative ordinale  |
| Qualitative binaire   |
| Temporelle            |


- La collecte des données (en quantité, qualité et variété suffisante)
- Le nettoyage de ces données (données manquantes, outliers values)
- Le prétraitement (visualisation, identifications des variables pertinentes)
- L'instanciation du modèle
- L'entrainement du modèle
- Les tests et la validation du modèle

<br>
<h3> <ins>  &#8658; Les algorithmes et les modèles </ins> </h3> 

<div align="justify">
Dans le cadre de l'apprentissage supervisé, nous distinguerons les problématiques de classification de celle de régression. 
En effet, la prédiction d'une variable quantitative se rapporte à une problématique de régression, tandis que la prédiction d'une variable qualitative (classe) est un problème de classification (clustering.
Cette dichotomie s'accompagne de différents algorithmes plus adaptés à chacun des cas. 
</div>

<br>

Algorithmes de régression : 
- Régression linéaire simple
- Régression linéaire multiple
- Régression polynomiale
- Régression à vecteur de support
- Arbre de décisions et Random Forest appliqués a la régression

Algorithmes de classification :
- Régression logistique (oui)
- KNN (K-nearest neighbours)
- SVM (support vector machine)
- Naives Bayes
- Decision Tree Classification
- Random Forest Classification


| Algorithme                   | Explication                                                                                            | Avantages                                                                                                                                 | Inconvénients                                                                                                                                                |
|------------------------------|--------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Régression logistique        | Régression linéaire avec fonction d'interpolation logistique                                           | Prediction rapide<br>Peu de risque de surapprentissage                                                                                    | Fonctionne uniquement dans le cadre d'une classification binaire,<br>avec des variables d'entrée indépedantes et des données complètes<br>Apprentissage long |
| KNN                          | Evalue les K plus proches voisins et en déduit le résultat de sortie                                   | Simple a implementer<br>Robuste au bruit (noise) du jeu de données d'entrainement                                                         | Choix du nombre de voisins                                                                                                                                   |
| Support Vector Machine - SVM | Cherche l'hyperplan qui sépare nos cluster de données                                                  | Evite la gestion des dimensions<br>Problèmes non linéaire complexes<br>Plus simple a entrainer que les réseaux de neurones                | Choix du noyau, complexité/cout important                                                                                                                    |
| Naives Bayes                 | Théorie des probabilités conditionnelles                                                               | Rapide, simple, adapté à une prédiction initiale                                                                                          | Considère inconditionnellement l'indépendance des variables explicatives entre elles                                                                         |
| Decision Tree Classification | Extrait des règles logiques en segmentant et associant des variables explicatives                      | Simple a comprendre et visualiser<br>Données numériques ou non                                                                            | Généralisation des branches parfois instable                                                                                                                 |
| Random Forest Classification | Multitude de Decision Tree en se debarassant du risque de sur apprentissage et de l'élagage de l'arbre | Un des meilleurs algo<br>Gestion données manquantes<br>Trouver les variables d'entrée les plus importantes (pondération, hyperparamètres) | Algo complexe, prédiction en temps réel lente                                                                                                                |
| Anomaly Detection            | -                                                                                                      | -                                                                                                                                         | -                                                                                                                                                            |
| Neurals Network              | -                                                                                                      | -                                                                                                                                         | -                                                                                                                                                            |
| Gradient Descent             | -                                                                                                      | -                                                                                                                                         | -                                                                                                                                                            |


<h3> <ins> &#8658; L'entrainement </ins> </h3>

<div align="justify">
Nos données d'entrainement données seront séparées aléatoirement en deux jeux de données distincts (60/40 ~ 80/20). 
Un échantillon complet (variables d'entrée et de sortie) sur lequel notre algorithme va s'entrainer, 
et un échantillon partiel (uniquement les valeurs d'entrée TODO C4EST FAUX) qui servira à la validation de l'entrainement du modèle.
En effet, une fois le modèle entrainé sur les 80% de données selectionnés, on lui passe nos différentes variables d'entrée des 20% du jeu de données de test, puis on compare les prédictions du modèle à la véritable sortie connue dans notre jeu de données initial.
</div>

<br>

<details>  
<summary> Architecture de code (lib scikit learning)</summary>

```python
import pandas as pd

from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn import model  # adapt

import pickle

from numpy import array


# Var
# x = variables, y = target
OUTPUT_VALUES = {
    0: 'Gibson',
    1: 'Fender'
}

# Data gathering
data = pd.read_csv('data.csv')
(entry_data, entry_class) = (data.iloc[:, :-1].values, data.iloc[:, -1].values)

# Labeled data splitting
x_train, x_test, y_train, y_test = train_test_split(entry_data, entry_class, test_size=.2)

# Training
model.fit(x_train, y_train)

# Validation : confusion matrice
confusion = confusion_matrix(y_test, model.predict(x_test))
print(confusion) # + metrics

# Validation : score
model.score(x_train, y_train)

# Save modele
pickle.dump(model, open('trained_model_save', 'wb'))

# Reload modele & run 
model = pickle.load(open('trained_model_save', 'rb'))
input_data = [17, 18]
y_pred = model.predict(
    array([input_data])
)
print(OUTPUT_VALUES[y_pred[0]])  # ie print 'Fender' if y_pred == 1 else 'Gibson'
```

</details>
<br>

<h3> <ins> &#8658; La validation et les tests </ins> </h3> 

<div align="justify">

Il existe différentes métriques afin de valider ou non l'entrainement d'un modèle.

<details>
<summary>Legende</summary>

```text
True Positive TP
False Positive FP
True Negative TN
False Negative FN

Positive = TP + FN
Negative = TN + FP
```

</details>

| Métrique             | Maths                                                                                                                                                                                          | Francais                                                                                                                                | Contexte                                            |
|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------|
| Matrice de confusion | -                                                                                                                                                                                              | Une matrice NxN <br> - pour chaque ligne une variable de sortie réelle/connue<br> - pour chaque colonne une variable de sortie estimée  |                                                     |
| Precision            | TP / ( TP + FP )                                                                                                                                                                               | On ne releve QUE des positifs quitte à en rater (si il dit A alors c'est A)                                                             | Une IA modérateur qui ban des utilisateurs          |
| Recall               | TP / (TP + FN)                                                                                                                                                                                 | On releve tous les positifs, quitte à prendre des négatifs                                                                              | Une IA médicale qui releve des cellules cancéreuses |
| f1score              | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;precision&nbsp;x&nbsp;recall&nbsp;)<br>2&nbsp;x&nbsp;────────────<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;precision&nbsp;+&nbsp;recall&nbsp;) | Moyenne harmonique entre la précision et le recall, qui prise séparement ne sont pas suffisantes pour évaluer l'entrainement d'un model |                                                     |

</div>

<details>

<summary>Méthodes de validation et de vérification</summary>

```python

def _custom_confusion():
    """
    True Positive TP
    False Positive FP
    True Negative TN
    False Negative FN
    
    Positive = TP + FN
    Negative = TN + FP
    
    TP_RATE = TP / (TP + FN ) = TP / Positive  # predict True & actual True
    TN_RATE = TN / ( TN + FP ) = TN / Negative  # predict False & actual False
    FP_RATE = FP / (FP + TN ) = FP / Negative  # predict True but actual False (type-1 error) 
    FN_RATE = FN / ( FN + TP ) = FN / Positive  # predict False but actual True (type-2 error)
    """

def _accuracy():
    """
    accuracy = (TP + TN ) / (TP + FP + TN + FN ) = ( TP + TN ) / (P + N)
    Evalue les predictions correctes sur l'ensemble de l'échantillon de test
    """

def _error_rate():
    """
    error_rate = ( FN + FP ) / (Positive + Negative)
    Ratio predictions incorrectes
    """

def _recall():
    """
    True Positive Rate
    recall = TP / (TP + FN)
    Flag tous les positifs quitte à se tromper (ie domaine médical)
    """
    
def _precision():
    """
    precision = TP / ( TP + FP )
    Flag QUE des positifs (ie bot modérateur qui ban des utilisateurs)
    """

def f1_score():
    """
    Moyenne harmonique precision & recall
    f1-score = ( 2 x Precision x Recall) / ( Precision + Recall )
    """
```
</details>

### TODO

ROC, renforcement, visu, svm from scratch, 
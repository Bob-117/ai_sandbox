import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation as anim

#
X = np.array([i for i in range(10)])
Y = np.array([2 * i for i in range(10)])

fig, ax = plt.subplots()
plt.scatter(X, Y, color='b')
# plt.plot(self.variables, Y_pred, color='g')
# plt.show()

x = np.arange(0, 2 * np.pi, .01)
line, = ax.plot(x, np.sin(x), c='yellow')


def animate(i):
    line.set_ydata(np.sin(x + i / 50))
    return line,


ani = anim.FuncAnimation(fig, animate, interval=20, blit=True, save_count=50)

plt.show()
# a, b, c, d = 1, 2, 3, 4
# e = f = g = h = 117
# i, j, k = [12] * 3
# print(a, b, c, d)
# print(e, f, g, h)
# print(i, j, k)

import os
import random

ext = ['zip', 'targz', 'py', 'txt', 'md']


def generate_data_22():
    current = os.getcwd()
    with open(f'{current}/src/data/data22.csv', 'w') as data_csv_file:
        print(os.getcwd())
        # metadata1,metadata2,Class
        nb_line = 999
        for i in range(nb_line):
            given_class = (0 if i < nb_line / 2 else 1)
            metadata1 = random.randrange(-100, 20, 1) if i < nb_line / 2 else random.randrange(-20, 100, 1)
            metadata2 = (random.randrange(0, 40, 1)) if i < nb_line / 2 else (random.randrange(60, 100, 1))
            current_line = f'{metadata1},{metadata2},{given_class}\n'
            data_csv_file.write(current_line)


def generate_data_33():
    current = os.getcwd()
    with open(f'{current}/src/data/data33.csv', 'w') as data_csv_file:
        print(os.getcwd())
        # metadata1,metadata2,Class
        nb_line = 999
        for i in range(nb_line):
            given_class = int(i // (nb_line / 3))
            metadata1 = random.randrange(-100, 20, 1) if 0 < i < nb_line / 3 else random.randrange(-20, 100, 1)
            metadata2 = (random.randrange(0, 40, 1)) if nb_line / 3 < i < 2 * nb_line / 3 else (random.randrange(60, 100, 1))
            metadata3 = (random.randrange(100, 200, 1)) if 2 * nb_line / 3 < i < nb_line else (random.randrange(-200, -100, 1))
            current_line = f'{metadata1},{metadata2},{metadata3},{given_class}\n'
            data_csv_file.write(current_line)


def generate_fish():
    current = os.getcwd()
    with open(f'fish.csv', 'w') as data_csv_file:
        print(os.getcwd())
        # metadata1,metadata2,Class
        nb_line = 999
        for i in range(nb_line):
            given_class = int(i // (nb_line / 3))
            metadata1 = random.randrange(-100, 20, 1) if 0 < i < nb_line / 3 else random.randrange(-20, 100, 1)
            metadata2 = (random.randrange(0, 40, 1)) if nb_line / 3 < i < 2 * nb_line / 3 else (random.randrange(60, 100, 1))
            metadata3 = (random.randrange(100, 200, 1)) if 2 * nb_line / 3 < i < nb_line else (random.randrange(-200, -100, 1))
            current_line = f'{metadata1},{metadata2},{metadata3},{given_class}\n'
            data_csv_file.write(current_line)

# generate_data_22()
# generate_data_33()
generate_fish()

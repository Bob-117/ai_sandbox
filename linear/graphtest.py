from matplotlib import pyplot as plt
from collections import Counter

from linear.src.SVMModel import SVMModel

colors = {
    0: 'blue',
    1: 'green',
    2: 'yellow',
    3: 'purple',
    4: 'pink'
}


def select_graph_data(model, mode):
    """
    return model.train or model.test
    :param model:
    :param mode:
    :return:
    """
    if mode == 'train':
        return model.var_train, model.target_train
    if mode == 'test':
        return model.var_test, model.target_test
    else:
        return model.variable, model.target


def graph_2d(var, target, axes):
    print('2d')
    print(f'{var.shape}, {target.shape}, {axes}')
    fig, ax = plt.subplots()
    for var, target in zip(var, target):
        # current_size = Graph.size_training_3d_graph(current_size, var[0], var[1], var[2])
        ax.scatter(var[0], var[1], color=colors.get(target[0], 'black'), label=target[0], zorder=1)
    ax.set_title(f"Training with {len(var)} sample", fontsize=10)
    ax.set_xlabel(model.features_list[0])
    ax.set_xlabel('model.features_list[0]')
    ax.set_ylabel(model.features_list[1])
    print('end')
    return fig, ax


def graph_3d(var, target, axes):
    print('3d')
    print(f'{var.shape}, {target.shape}, {axes}')
    target = model.target
    var = model.variable
    axes = model.features_list

    if target.shape != (len(var), 1):
        target = target.reshape(len(var), 1)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.set_xlabel(axes[0])
    ax.set_ylabel(axes[1])
    ax.set_zlabel(axes[2])

    current_size = [0, 0, 0, 0, 0, 0]
    for var, target in zip(var, target):
        # current_size = Graph.size_training_3d_graph(current_size, var[0], var[1], var[2])
        ax.scatter(var[0], var[1], var[2], color=colors.get(target[0], 'black'), label=target[0], zorder=1)
    labels = sorted([f'{item}: {count}' for item, count in Counter(target).items()])
    ax.legend(labels, loc='lower right', title='legend')
    # print(list(map(Graph.round_hundred, current_size)))
    # ax.legend(['a', 'b', 'c'], Graph.colors)

    plt.show()
    return plt


def graph_confusion():
    pass


def graph_precision_recall():
    pass


def training_graph(model):
    print(f'training graph - {model.variable.shape}')
    if model.variable.shape[1] in [2, 3]:

        fig, ax = plt.subplots(ncols=2, nrows=2, figsize=(10, 5))
        ax[0][0].set_title('training data')
        HEHE = graph_2d(model.variable, model.target, model.features_list)
        print(f'hehe _{HEHE}')
        ax[0][0] = graph_2d(model.variable, model.target, model.features_list)
        ax[0][1].set_title('validation data')

        ax[1][0].set_title('precision recall')
        ax[1][1].set_title('confusion')
        plt.show()
    else:
        fig, ax = plt.subplots(ncols=2, nrows=1, figsize=(10, 5))
        ax[0].set_title('precision recall')
        ax[1].set_title('confusion')

    fig.suptitle(f'training for {model.data}')
    plt.show()


def predict_graph(model):
    # graph + add point
    print(f'predict graph - {model.variable.shape}')
    pass


def default_graph(model):
    print(f'default graph - {model.variable.shape}')


def draw_graph(model, mode):
    graphs = {
        'train': training_graph,
        'predict': predict_graph
    }

    graph = graphs.get(mode, default_graph)
    graph(model)


if __name__ == '__main__':

    svc_file_22 = SVMModel(file='src/data/data22.csv')
    svc_file_33 = SVMModel(file='src/data/data33.csv')
    svc_default = SVMModel()
    svc_default_dim = SVMModel(dim=[5, 5])

    models = [svc_file_22, svc_file_33, svc_default, svc_default_dim]

    for model in models:
        model.train()
        draw_graph(model, 'train')
        draw_graph(model, 'predict')

        # print(type(model.variable))
        # print(model.variable.shape[1])

    #
    # svc_data = SVMModel(data='iris')
    #

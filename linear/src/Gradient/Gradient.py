import matplotlib.colors
import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import make_blobs



class Gradient:
    u"""
    Rappel regression lineaire :

    Trouver la droite qui s'approche le plus des datas

    Univariée : feature x en entrée, fonction de prediction h(x) en sortie
        h(x) = \u03B8\u2080 + \u03B8\u2081 * x

    Mutlivariée : generalisation du modele de regression lineaire, avec plusieurs features
        h(x) = \u03B8\u2080 + \u03A3 (\u03B8\u1D62 * x\u1D62)

    Traitement des datas et correlation :
        - Choix de n features qui contribuent le plus a la precision du modele
        - Les 2 features les plus corrélées
        - La feature la plus correlée avec le reste
        - Relever des grappes de features corrélées

    Problematique : Calculer les coefficients de la droite de regression
         - Erreur unitaire = (h(x\u1D62) - y\u1D62)²
         - On cherche a minimiser \u03A3 (h(x\u1D62) - y\u1D62)²   0 < i < m la taille du dataset

            Fonction de cout J(\u03B8\u2080, \u03B8\u2081) :

            1/2m \u03A3 (h(x\u1D62) - y\u1D62)² = 1/2m \u03A3 (\u03B8\u2080 + \u03B8\u2081 * x\u1D62 - y\u1D62)²


        Minimisation de J(\u03B8\u2080, \u03B8\u2081)

    """

    def __init__(self):
        self.name = 'Gradient'
        # data
        self.variable = None
        self.target = None
        self.features_list = None
        # init
        self.learning_rate = float(.01)
        self.initial_theta_0 = float(0)
        self.initial_theta_1 = float(0)
        self.nb_iter = 200
        self.COST_RECORDER = []

    def load_data(self):
        """

        :return:
        """
        (self.variable, self.target) = make_blobs(n_samples=1000, n_features=1, centers=4)
        self.features_list = [f'feature{i}' for i, feature in enumerate(self.variable)]
        # print('----------')
        # print(self.variable)
        # print(self.target)
        # print(self.variable.shape, self.variable.shape[0], self.variable.shape[1], self.target.shape)
        # print('----------')

        self.draw()
        return self.variable, self.target.reshape(len(self.target), 1)

    def cost_function(self, theta_0, theta_1):
        """
        Calcul du cout global pour \u03B8\u2080 & \u03B8\u2081 fixé
        Le but etant de minimiser le cout en ajustant les coefficients de notre regression lineaire
        :param theta_0:
        :param theta_1:
        :return:
        """
        global_cost = 0
        for i in range(len(self.variable)):
            cost_i = ((theta_0 + (theta_1 * self.variable[i])) - self.target[i]) * (
                    (theta_0 + (theta_1 * self.variable[i])) - self.target[i])
            global_cost += cost_i
        return (1 / (2 * len(self.variable))) * global_cost

    def partial_derivative(self, ancien_theta_0, ancien_theta_1):
        """

        :return:
        """
        M = len(self.variable)
        derivative_theta_0 = float(0)
        derivative_theta_1 = float(0)
        for i in range(M):
            derivative_theta_0 += ((ancien_theta_0 + (ancien_theta_1 * self.variable[i])) - float(self.target[i]))
            derivative_theta_1 += (
                                          (ancien_theta_0 + (ancien_theta_1 * self.variable[i])) - float(self.target[i])
                                  ) * float(self.variable[i])
        derivative_theta_0 = 1 / M * derivative_theta_0
        derivative_theta_1 = 1 / M * derivative_theta_1
        return [derivative_theta_0, derivative_theta_1]

    def update_new_theta(self, ancien_theta_0, ancien_theta_1):
        """

        :return:
        """
        [derivative_theta_0, derivative_theta_1] = self.partial_derivative(ancien_theta_0, ancien_theta_1)
        new_theta_0 = ancien_theta_0 - (self.learning_rate * derivative_theta_0)
        new_theta_1 = ancien_theta_1 - (self.learning_rate * derivative_theta_1)
        self.COST_RECORDER.append(self.cost_function(new_theta_0, new_theta_1))
        return [new_theta_0, new_theta_1]

    def run_gradient(self):
        """
        Pour N itérations, on calcul les coefficients de la regresison lilneaire en minimisant J (= 0=
        :return:
        """
        tmp_theta_0, tmp_theta_1 = self.initial_theta_0, self.initial_theta_1
        for i in range(self.nb_iter):
            [new_theta_0, new_theta_1] = self.update_new_theta(tmp_theta_0, tmp_theta_1)
            tmp_theta_0, tmp_theta_1 = new_theta_0, new_theta_1
        return [tmp_theta_0, tmp_theta_1]

    def draw(self):

        # Graph.display_2d_graph(self.variable, self.target.reshape(len(self.target), 1))
        # ax = fig.gca(projection='3d')
        plt.plot(self.variable, self.target.reshape(len(self.target), 1), 'ro')
        plt.xlabel('feature X')
        plt.ylabel('target Y')
        plt.show()

    def draw_gradient(self, colorsMap='jet'):

        # cm = plt.get_cmap(colorsMap)
        # cNorm = matplotlib.colors.Normalize(vmin=min(cs), )
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        # gridX, gridY = np.mgrid[-4:4:33 * 1j, -4:4:33 * 1j]
        gridX, gridY = np.mgrid[-10:11:1, -10:11:1]
        Z = self.cost_function(gridX, gridY)
        # for i in range(-10, 10, 1):
        #     for j in range(-10, 10, 1):
        #         ax.plot_surface(i, j, Z)

        ax.plot_surface(gridX, gridY, Z, alpha=0.2)

        return ax, plt


if __name__ == '__main__':
    gradient = Gradient()
    gradient.load_data()
    gradient.draw_gradient()
    gradient.run_gradient()
    plt.show()
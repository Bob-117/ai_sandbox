import matplotlib.pyplot as plt
import numpy as np


class LinearRegression:

    def __init__(self, variables, target):
        self.variables = variables
        self.target = target
        self.coeff = [0, 0]  # y = alpha * x + beta
        self.save = 0

    def update_coeff(self, learning_rate):
        Y_pred = self.predict()
        Y = self.target
        m = len(Y)
        self.coeff[0] = self.coeff[0] - (learning_rate * ((1 / m) * np.sum(Y_pred - Y)))
        self.coeff[1] = self.coeff[1] - (learning_rate * ((1 / m) * np.sum((Y_pred - Y) * self.variables)))

    def predict(self, X=None):
        Y_pred = np.array([])
        X = X or self.variables
        coeff = self.coeff
        for x in X:
            Y_pred = np.append(Y_pred, coeff[0] + (coeff[1] * x))
        # print(Y_pred)
        return Y_pred

    def get_current_accuracy(self, Y_pred):
        predict_target, known_target = Y_pred, self.target
        n = len(Y_pred)
        return 1 - sum(
            [
                abs(predict_target[i] - known_target[i]) / known_target[i]
                for i in range(n)
                if known_target[i] != 0
            ]
        ) / n

    def compute_cost(self, Y_pred):
        m = len(self.target)
        J = (1 / (2 * m)) * (np.sum((Y_pred - self.target) ** 2))
        # if J > self.save:
        #     print(f'STOP : best coeff : {self.coeff}')
        if J > self.save != 0:
            print(f'stop : {J}')
        self.save = J
        return J

    def plot_best_fit(self, Y_pred, fig):
        # print(self.coeff)
        #
        # # plt.plot(self.variables, self.target.reshape(len(self.target), 1), 'ro')
        # # plt.xlabel('feature X')
        # # plt.ylabel('target Y')
        # # plt.show()
        #
        # # aaaaaa = plt.figure()
        # # # plt.scatter(self.variables, self.target, color='red')
        # # # plt.plot(self.variables, Y_pred, color='blue')
        # f = plt.figure(fig)
        # plt.scatter(self.variables, self.target, color='b')
        # # plt.plot(self.variables, Y_pred, color='g')
        #
        # plt.plot(self.variables, list(map(lambda feature: self.coeff[0] * feature + self.coeff[1], self.variables)))
        #
        # # for i in self.variables:
        # #     plt.plot(i, self.coeff[0] * i + self.coeff[1], color='yellow')
        # # plt.plot()
        # plt.title(f'{self.coeff}')
        # plt.show()
        f = plt.figure(fig)
        plt.scatter(self.variables, self.target, color='b')
        # plt.plot(self.variables, Y_pred, color='g')
        plt.plot(self.variables, list(map(lambda feature: self.coeff[0] * feature + self.coeff[1], self.variables)))
        plt.show()



def run_linear_regression():
    X = np.array([i for i in range(10)])
    Y = np.array([2 * i for i in range(10)])
    # Y = np.random.rand(10)

    x = [10, 8, 13, 9, 11, 14, 6, 4, 12, 7, 5]
    y1 = [8.04, 6.95, 7.58, 8.81, 8.33, 9.96, 7.24, 4.26, 10.84, 4.82, 5.68]

    X = x
    Y = y1
    regressor = LinearRegression(X, Y)

    iterations = 0
    steps = 1000
    learning_rate = .001
    costs = []

    Y_pred = regressor.predict()
    regressor.plot_best_fit(Y_pred=Y_pred, fig='initial best line fit')

    while 1:
        tmp_Y_pred = regressor.predict()
        cost = regressor.compute_cost(Y_pred=tmp_Y_pred)
        costs.append(cost)
        regressor.update_coeff(learning_rate=learning_rate)

        iterations += 1

        if iterations % steps == 0:
            print(f'{iterations} epochs elapsed')
            print(f'Current accuracy is : {regressor.get_current_accuracy(Y_pred=tmp_Y_pred)}')
            regressor.plot_best_fit(Y_pred=tmp_Y_pred, fig='Temp fit line')
            stop = input('do u want to stop (y/*) ?')
            if stop in ('y', 'yes', 'oui', 'o'):
                break

    regressor.plot_best_fit(Y_pred=tmp_Y_pred, fig='Final best fit line')

    h = plt.figure('Verification')
    plt.plot(range(iterations), costs, color='b')
    h.show()

    # regressor.predict(X=[i for i in range(10)])
    regressor.predict(X=[12])


if __name__ == '__main__':
    run_linear_regression()

import matplotlib.pyplot as plt

from src import AIModel, Graph
from sklearn.svm import SVC


class SVMModel(AIModel):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.classifier = SVC()





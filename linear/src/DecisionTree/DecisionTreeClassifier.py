import math

import numpy as np
import pandas as pd

from linear.src.DecisionTree.Node import Node


# https: // towardsdatascience.com/ id3-decision-tree-classifier-from-scratch-in-python-b38ef145fd90

class DecisionTreeClassifier:
    """
    Decision Tree Classifier using ID3 algorithm
    """

    def __init__(self, features, features_name, targets):
        self.features = features
        self.features_name = features_name
        self.targets = targets
        self.targetsList = list(set(targets))
        self.node = None
        self.entropy = self._get_entropy([x for x in range(len(self.targets))])

    def _get_entropy(self, features_ids):

        # Sort targets by isntance id
        targets = [self.targets[i] for i in features_ids]

        # count number of instance of each target
        targets_count = [targets.count(name) for name in self.features_name]

        # calculate the entropy for each target and sum them
        entropy = sum(
            [
                -count / len(features_ids) * math.log(count / len(features_ids), 2)
                if count else 0
                for count in targets_count
            ]
        )
        # H(S) = SUM(i=1,n)(-p_i log2(p_i)), p_i = proportion of each target
        return entropy

    def _get_information_gain(self, x_ids, feature_ids):

        # total entropy
        info_gain = self._get_entropy(x_ids)

        # store in a list all the values of the chosen feature

        # x_features = [self.features[x][feature_ids] for x in x_ids]
        x_features = [self.features[x] for x in x_ids]

        # get unique values
        features_vals = list(set(x_features))

        # get frequency for each value
        feature_v_count = [self.features.count(x) for x in features_vals]

        # get the feature values id
        feature_v_id = [
            [
                x_ids[i]
                for i, x in enumerate(x_features)
                if x == y
            ]
            for y in features_vals
        ]

        # compute the information gain with the chosen feature
        info_gain_feature = sum(
            [
                v_counts / len(x_ids) * self._get_entropy(v_ids) for v_counts, v_ids in
                zip(feature_v_count, feature_v_id)
            ]
        )

        info_gain = info_gain - info_gain_feature

        return info_gain

    def _get_feature_max_information_gain(self, x_ids, feature_ids):
        # get entropy for each feature
        features_entropy = [
            self._get_information_gain(x_ids=x_ids, feature_ids=feature_ids) for feature_id in feature_ids
        ]

        # find the feature that maximises the information gain
        max_id = feature_ids[features_entropy.index(max(features_entropy))]

        return self.features_name[max_id], max_id

    def id3(self):
        # assign unique number to each instance
        x_ids = [x for x in range(len(self.features))]
        # assign unique number to each feature
        feature_ids = [x for x in range(len(self.features_name))]
        self.node = self._id3_recv(x_ids, feature_ids, self.node)

    def _id3_recv(self, x_ids, feature_ids, node):

        if not node:
            node = Node()
        labels_in_features = [
            self.targets[x] for x in x_ids
        ]

        if len(set(labels_in_features)) == 1:
            node.value = self.targets[x_ids[0]]
            return node

        if len(feature_ids) == 0:
            node.value = max(set(labels_in_features), key=labels_in_features.count)
            return node

        # else choose best feature
        best_feature_name, best_feature_id = self._get_feature_max_information_gain(x_ids, feature_ids)
        node.value = best_feature_name
        node.children = []

        # feature_values = list(set([self.features[x][best_feature_id] for x in x_ids]))
        feature_values = list(set([self.features[x] for x in x_ids]))

        for value in feature_values:
            child = Node()
            child.value = value
            node.children.append(child)

            child_x_ids = [
                # x for x in x_ids if self.features[x][best_feature_id] == value
                x for x in x_ids if self.features[x] == value
            ]

            if not child_x_ids:
                child.next = max(set(labels_in_features), key=labels_in_features.count)
                print('test')
            else:
                if feature_ids and best_feature_id in feature_ids:
                    to_remove = feature_ids.index(best_feature_id)
                    feature_ids.pop(to_remove)

                child.next = self._id3_recv(child_x_ids, feature_ids, child.next)
        return node

    def generate_data(self):
        print(self.features)
        data = {
            'wind_direction': ['N', 'S', 'E', 'W'],  # feature
            'tide': ['Low', 'High'],  # feature
            'swell_forecasting': ['small', 'medium', 'large'],  # feature
            'good_waves': ['Yes', 'No']  # target
        }

        # create an empty dataframe
        data_df = pd.DataFrame(columns=data.keys())

        np.random.seed(117)

        for i in range(100):
            data_df.loc[i, 'wind_direction'] = str(np.random.choice(data['wind_direction'], 1)[0])
            data_df.loc[i, 'tide'] = str(np.random.choice(data['tide'], 1)[0])
            data_df.loc[i, 'swell_forecasting'] = str(np.random.choice(data['swell_forecasting'], 1)[0])
            data_df.loc[i, 'good_waves'] = str(np.random.choice(data['good_waves'], 1)[0])

        print(data_df.head())


features = [1, 1, 2, 55, 8, 3]
features_name = ['ok', 'pasok', 'presque', 'oui', 'non', 'bug']
targets = ['ok', 'pasok', 'presque', 'oui', 'non', 'bug']

a = DecisionTreeClassifier(features=features, features_name=features_name, targets=targets)
a.generate_data()
a.id3()

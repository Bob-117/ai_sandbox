class Node:
    """
    Contains the information of the node and another nodes of the decision tree
    """

    def __init__(self):
        self.value = None  # feature/variable to make the split and branches
        self.next = None  # next node
        self.children = None  # Branches coming off the decision nodes


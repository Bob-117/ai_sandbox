import logging
from abc import ABC

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from numpy import array
from sklearn import datasets
from sklearn.datasets import make_blobs
from sklearn.metrics import confusion_matrix, precision_score
from sklearn.model_selection import train_test_split

from .Graph import Graph


class AIModel(ABC):

    def __init__(self, **kwargs):
        # initial training dataset & classifier
        self.data = kwargs.get('data') or None
        self.file = (kwargs.get('file') or None) if not self.data else None
        self.dim = kwargs.get('dim') or None
        self.classifier = None
        self.features_list = []
        # training data
        self.variable = None
        self.target = None
        self.var_train = None
        self.var_test = None
        self.target_train = None
        self.target_test = None
        # validation
        self.confusion = None
        self.recall = None
        self.f1_score = None

    def load_data_from_file(self):
        """
        load dataset from csv file
        :return:
        """
        data = pd.read_csv(self.file)
        self.features_list = list(data.keys())
        (variable, target) = (data.iloc[:, :-1].values, data.iloc[:, -1].values)
        self.variable, self.target = variable, target.reshape(len(target), 1)  # always handle m*n array
        return self.variable, self.target

    def load_data_from_scikit_datasets(self):
        """
        load dataset from default scikit datasets
        :return:
        """
        iris = datasets.load_iris()
        # data = pd.DataFrame(data.data, columns=data.feature_names)
        self.features_list = iris.feature_names
        (self.variable, self.target) = iris.data, iris.target.reshape(len(iris.target), 1)
        return self.variable, self.target

    def load_data(self, dim=None):
        """
        load data from file.csv or from scikit datasets
        file has priority
        """
        if self.file:
            return self.load_data_from_file()
        elif self.data:
            return self.load_data_from_scikit_datasets()
        else:
            self.data = 'default data'
            return self.load_default_data()

    def load_default_data(self):
        """
        load default data samples with 2 features & 2 targets
        :return:
        """
        if not self.dim:
            self.dim = [2, 2]
        (self.variable, self.target) = make_blobs(n_samples=1000, n_features=self.dim[0], centers=self.dim[1])
        self.features_list = [f'feature{i}' for i, feature in enumerate(self.variable)]
        return self.variable, self.target.reshape(len(self.target), 1)

    def split_data(self, test_size=.2):
        """
        split dataset between :
        - variable_train ie x_train
        - target_train ie y_train
        - variable_test ie x_test
        - target_train ie y_test
        """
        entry_var, entry_target = self.load_data()
        self.var_train, self.var_test, self.target_train, self.target_test = train_test_split(
            entry_var, entry_target, test_size=test_size
        )
        # print(list(map(lambda elem: len(elem), [self.var_train, self.var_test, self.target_train, self.target_test])))
        return [self.var_train, self.var_test, self.target_train, self.target_test]

    # def graph_2d(self):
    #     """
    #     display our dataset
    #     useful to identify outlier data
    #     :return:
    #     """
    #     return Graph.display_2d_graph(self.variable, self.target)

    def graph_3d(self):
        """
        display our dataset
        useful to identify outlier data
        :return:
        """
        return Graph.display_3d_graph(self.variable, self.target, self.features_list)

    def train(self):
        """
        split data (x_train, y_train, x_test, y_test)
        train model with classifier.fit(x_train, y_train)
        display 2d or 3d graph depending on features number (x)
        :return:
        """
        self.split_data()
        self.classifier.fit(self.var_train, self.target_train.reshape(len(self.target_train, )))
        score = self.classifier.score(self.var_test, self.target_test)
        print(f'-----TRAINING RESULTS with {self.classifier} model for {self.file if self.file else self.data}-------')
        print(
            f'nb_sample: {len(self.variable)} | {len(self.target)}, train : {len(self.var_train)} samples, test : {len(self.var_test)} samples')
        print(f'Score de {score}, {"Success training" if score > .8 else "Failed training => need more/better data"}')
        # print(f'{self.precision()}')
        # print(f'{self.confusion_matrix()}')
        self.confusion_matrix()
        self.generate_training_graph()
        return self.classifier

    def save(self):
        """
        pickle.dump()
        :return:
        """
        pass

    def load(self):
        """
        pickle.load()
        :return:
        """
        pass

    def confusion_matrix(self):
        self.confusion = confusion_matrix(self.target_test, self.classifier.predict(self.var_test))
        return self.confusion

    def custom_confusion(self):
        """
        True Positive TP
        False Positive FP
        True Negative TN
        False Negative FN

        Positive = TP + FN
        Negative = TN + FP

        TP_rate = TP / (TP + FN) = TP / Positive
        FP_rate = FP / (FP + TN) = FP / Negative
        TN_rate = TN / (TN + FP) = TN / Negative
        FN_rate = FN / (FN + TP) = FN / Positive

        good classifier = high TP & TN
        [[10, 1],
        [1, 10]]
        good classifier = high TP_rate, high TN_rate, low FP_rate & low FN_rate
        """
        if len(np.unique(self.target)) == 2:  # If targets in {0, 1}
            TP = FP = TN = FN = 0
            for y, y_hat in zip(self.target_test, self.classifier.predict(self.var_test)):
                if y == 1 and y_hat == 1:
                    TP += 1
                elif y == 0 and y_hat == 0:
                    TN += 1
                elif y == 1 and y_hat == 0:
                    FN += 1
                elif y == 0 and y_hat == 1:
                    FP += 1

            return np.array([
                [TN, FP],
                [FN, TP]
            ])
        else:
            logging.info("More than 2 unique targets, using sklearn confusion method")
            return self.confusion_matrix()

    def precision(self):
        print('--------PRECISION---------')
        print(precision_score(self.target_test, self.classifier.predict(self.var_test), average=None))
        print(precision_score(self.target_test, self.classifier.predict(self.var_test), average='weighted'))
        print('----------------------')

        # a = precision_score(self.target_test, self.classifier.predict(self.var_test), average='weighted')
        # print(a)
        return precision_score(self.target_test, self.classifier.predict(self.var_test), average='weighted')

    def precision_recall_score(self):
        """
        Precision = 0 < TP / (TP + FP) < 1
        for all points declared positive, what percentage are actually positive

        Recall = 0 < TP / (TP + FN) = TP / Positive < 1
        for all points actually positive, what percentage was the model able to detect/predict
        """
        pass

    def f1_score(self):
        """
        f1_score = 0 < 2 * (precision * recall) / (precision + recall) < 1
        higher = better
        :return:
        """
        pass

    def predict(self, data):
        """
        target_predict = model.predict(data) = [0] | [1]
        based on previous training
        :param data:
        :return:
        """

        return self.classifier.predict(
            array([data])
        )

    def run(self, data):
        """
        Run the IA prediction on a new data entry
        :param data:
        :return:
        """
        print(f'-----RUNNING PREDICTION for {data} '
              f'based on {self.file if self.file else self.data} '
              f'training (model : {self.classifier})-------')
        result = self.predict(data)
        self.generate_predict_graph(data).show()
        print(f'We estimate {data} to be target {result[0]} with {self.precision()}%')
        return result[0]

    def generate_training_graph(self):
        if self.variable.shape[1] == 2:
            # Only 2 features
            Graph.train_test_side_by_side(self.var_train, self.target_train, self.var_test, self.target_test,
                                          features_list=self.features_list, confusion=self.confusion, pred=self.classifier.predict(self.var_test))
        elif self.variable.shape[1] == 3:
            # 3 features
            graph = self.graph_3d()
            graph.show()
        else:
            # more features
            print(f'Un graph en plus de 3 dimensions n\'est pas très pertinent : {self.features_list}')

    def generate_predict_graph(self, data):
        """
        generate the prediction graph with training data + new data
        :param data:
        :return:
        """
        print(f'GENERATE PREDICT GRAPH')
        # print(data)
        # print(type(data))
        # print(len(data))
        # print(data.shape)
        # print(data.shape[0])
        # print(data.shape[1])
        if len(data) == 2:
            # if data.shape == (1, 2):
            graph = Graph.display_2d_graph(self.variable, self.target)
            graph.scatter(data[0], data[1], c="red")
        elif len(data) == 3:
            # graph = Graph.display_3d_graph(self.variable, self.target, self.features_list)
            graph = self.graph_3d()
            graph.scatter(data[0], data[1], data[2], c="red", zorder=1)
        else:
            graph = plt
        graph.title(f"PREDICT {data}")
        # updated_size = Graph.size_graph_predict(graph.axis(), data)
        # graph.axis(updated_size)
        return graph

import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def neural_network(variables, targets):
    """
    2 layers (layer1 hidden, output layer)
    :param variables:
    :param targets:
    :return:
    """
    global output
    learning_rate = .01
    w1 = np.random.rand(2, 4)  # weights that connect input w/ layer1
    w2 = np.random.rand(4, 1)  # weights that connect mayer1 w/ output

    for epoch in range(10000):
        layer1 = sigmoid(np.dot(variables, w1))
        output = sigmoid(np.dot(layer1, w2))

        # apply changes to our layers
        error = targets - output  # we dont care abt square, all we need  is the derivative_error 2 * (target - output)

        delta2 = 2 * error * (output * (1 - output))
        delta1 = delta2.dot(w2.T) * (layer1 * (1 - layer1))

        w2 += learning_rate * layer1.T.dot(delta2)
        w1 += learning_rate * variables.T.dot(delta1)

    return np.round(output).flatten()


var = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])

print("OR", neural_network(var, np.array([[0, 1, 1, 1]]).T))
print("AND", neural_network(var, np.array([[0, 0, 0, 1]]).T))
print("XOR", neural_network(var, np.array([[0, 1, 1, 0]]).T))
print("NAND", neural_network(var, np.array([[1, 1, 1, 0]]).T))
print("NOR", neural_network(var, np.array([[1, 0, 0, 0]]).T))


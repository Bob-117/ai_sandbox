from collections import Counter

from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, precision_recall_curve


class Graph:
    colors = {
        0: 'blue',
        1: 'green',
        2: 'yellow',
        3: 'purple',
        4: 'pink'
    }

    @staticmethod
    def display_2d_graph(var, target):
        """
        display our dataset
        useful to identify outlier data
        :return:
        """
        nb_sample = len(var)
        current_size = [0, 0, 0, 0]
        for var, target in zip(var, target):
            current_size = Graph.size_training_2d_graph(current_size, var[0], var[1])
            plt.scatter(var[0], var[1], color=Graph.colors.get(target[0], 'black'), zorder=1)
        plt.axis(list(map(Graph.round_hundred, current_size)))
        return plt

    @staticmethod
    def display_3d_graph(var, target, axes):
        """
        display our dataset
        useful to identify outlier data
        :return:
        """
        if target.shape != (len(var), 1):
            target = target.reshape(len(var), 1)

        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')
        ax.set_xlabel(axes[0])
        ax.set_ylabel(axes[1])
        ax.set_zlabel(axes[2])

        current_size = [0, 0, 0, 0, 0, 0]
        for var, target in zip(var, target):
            current_size = Graph.size_training_3d_graph(current_size, var[0], var[1], var[2])
            ax.scatter(var[0], var[1], var[2], color=Graph.colors.get(target[0], 'black'), label=target[0], zorder=1)
        labels = sorted([f'{item}: {count}' for item, count in Counter(target).items()])
        ax.legend(labels, loc='lower right', title='legend')
        # print(list(map(Graph.round_hundred, current_size)))
        # ax.legend(['a', 'b', 'c'], Graph.colors)

        return plt

    @staticmethod
    def size_training_2d_graph(current_size, width, height):
        """
        resize graph to be sure we display all points
        useful to identify outlier data
        :param current_size:
        :param width:
        :param height:
        :return:
        """
        min_w = current_size[0]
        max_w = current_size[1]
        min_h = current_size[2]
        max_h = current_size[3]
        if width < min_w:
            min_w = width
        if width > max_w:
            max_w = width
        if height < min_h:
            min_h = height
        if height > max_h:
            max_h = height
        return [min_w, max_w, min_h, max_h]

    @staticmethod
    def size_training_3d_graph(current_size, width, height, depth):
        """
        resize graph to bbe sure we display all points
        useful to identify outlier data
        :param current_size:
        :param width:
        :param height:
        :param depth:
        :return:
        """
        min_x = current_size[0]
        max_x = current_size[1]
        min_y = current_size[2]
        max_y = current_size[3]
        min_z = current_size[4]
        max_z = current_size[5]
        if width < min_x:
            min_x = width
        if width > max_x:
            max_x = width
        if height < min_y:
            min_y = height
        if height > max_y:
            max_y = height
        if depth < min_z:
            min_z = depth
        if depth > max_z:
            max_z = depth

        return [min_x, max_x, min_y, max_y, min_z, max_z]

    @staticmethod
    def size_graph_predict(current_size, new_data):
        """
        Make sure that we can see the new dot on graph
        :param current_size:
        :param new_data:
        :return:
        """
        return [
            min(current_size[0], new_data[0]) - 100,
            max(current_size[1], new_data[0]) + 100,
            min(current_size[2], new_data[1]) - 100,
            max(current_size[3], new_data[1]) + 100,
        ]

    @staticmethod
    def round_hundred(x):
        """
        round graph height & width to next hundred
        :param x:
        :return:
        """
        return x if x % 100 == 0 else x + 100 - x % 100

    @staticmethod
    def train_test_side_by_side(var_train, target_train, var_test, target_test, features_list, confusion, pred):
        """
        show training & testing model side by side
        :param confusion:
        :param features_list:
        :param var_train:
        :param target_train:
        :param var_test:
        :param target_test:
        :return:
        """
        fig, ax = plt.subplots(ncols=2, nrows=2, figsize=(10, 5))
        # fig, ax = plt.subplots(ncols=2, nrows=2, figsize=(10, 5), subplot_kw=dict(projection="polar"))
        for var, target in zip(var_train, target_train):
            # current_size = Graph.size_training_3d_graph(current_size, var[0], var[1], var[2])
            ax[0][0].scatter(var[0], var[1], color=Graph.colors.get(target[0], 'black'), label=target[0], zorder=1)
        for var, target in zip(var_test, target_test):
            # current_size = Graph.size_training_3d_graph(current_size, var[0], var[1], var[2])
            ax[0][1].scatter(var[0], var[1], color=Graph.colors.get(target[0], 'black'), label=target[0], zorder=1)

        # conf_matrix = confusion_matrix(y_true=target_train, y_pred=target_test)

        ax[1][1].matshow(confusion, cmap=plt.cm.Blues, alpha=.3)
        for i in range(confusion.shape[1]):
            for j in range(confusion.shape[1]):
                ax[1][1].text(x=j, y=i, s=confusion[i, j], va='center', ha='center', size='xx-large')

        # TODO scikiplot.estimators.plot_learning_curve()

        fig.suptitle("Training & validation graph", fontsize=16)

        # fig.tight_layout()
        # plt.subplots_adjust(left=.1, right=.1)
        # plt.subplot_tool()
        # plt.subplots_adjust(left=.5)
        ax[0][0].set_title(f"Training with {len(var_train)} sample", fontsize=10)
        ax[0][0].set_xlabel(features_list[0])
        ax[0][0].set_ylabel(features_list[1])
        ax[0][1].set_title(f"Test with {len(var_test)} sample", fontsize=10)
        ax[0][1].set_xlabel(features_list[0])
        ax[0][1].set_ylabel(features_list[1])

        # precision_recall = Graph.plot_precision_recall_vs_thresholds(target_test=target_test, pred=pred)
        # ax[1][0].matshow(a)
        precisions, recalls, thresholds = precision_recall_curve(y_true=target_test, probas_pred=pred, pos_label=1)
        ax[1][0].set_title('precision recall curve')
        ax[1][0].plot(thresholds, precisions[:-1], 'b--', label='Precision')
        ax[1][0].plot(thresholds, recalls[:-1], 'g--', label='Recall')
        ax[1][0].set_xlabel('Thresholds')
        ax[1][0].legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
        plt.show()

    @staticmethod
    def plot_precision_recall_vs_thresholds(target_test, pred):

        fig = plt.figure()
        precisions, recalls, thresholds = precision_recall_curve(y_true=target_test, probas_pred=pred)
        # print(precisions)
        # print(recalls)
        # print(thresholds)
        # prec_recal_graph = plt
        plt.plot(thresholds, precisions[:-1], 'b--', label='Precision')
        plt.plot(thresholds, recalls[:-1], 'g--', label='Recall')
        plt.xlabel('Thresholds')
        plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
        # plt.grid(b=True, which='both', axis='both', color='gray', linestyle='-', linewidth=1)
        # plt.legend()

        return fig

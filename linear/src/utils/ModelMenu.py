from src import SVCModel
from src.utils.ActionMenu import ActionMenu


class ModelMenu:

    @staticmethod
    def display():
        print('-- Choix du modele --')
        MODEL_LIST = ['svc', 'exit']

        model = input('modele : ')
        while model not in MODEL_LIST:
            model = input(f'modele parmi {MODEL_LIST}: ')
        if model == 'svc':
            model = SVCModel('data/svc_data.csv')
            ActionMenu.display(model)
        elif model == 'knn':
            model = 'a'
            ActionMenu.display(model)
        elif model == 'exit':
            exit()

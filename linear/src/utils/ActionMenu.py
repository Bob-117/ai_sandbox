class ActionMenu:

    @staticmethod
    def display(model):
        action = input(f' {model} => train or run : ')
        while action not in ['train', 'run' 'exit']:
            action = input(f' {model} => train or run : ')
        if action == 'train':
            model.train()
        elif action == 'run':
            new = input('New data (format X:int,Y:int) : ')
            model.run(new=new)
        elif action == 'exit':
            exit()

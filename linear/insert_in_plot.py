import random

import matplotlib.pyplot as plt
import numpy as np


def create_predef_plot(ax=None, model='default_title'):
    if not ax:
        f, ax = plt.subplots()
    ax.plot(np.random.randint(1, 10, size=(2, 2)))
    ax.set_title(model)
    return ax


main_fig, ((ax00, ax01), (ax10, ax11)) = plt.subplots(ncols=2, nrows=2, figsize=(10, 5))
create_predef_plot(ax=ax00, model='train')
create_predef_plot(ax=ax01, model='validation')
create_predef_plot(ax=ax10, model='recall precision')
create_predef_plot(ax=ax11, model='confusion')
plt.show()

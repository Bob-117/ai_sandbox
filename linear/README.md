# POC : IA de catégorisation de malware
## *Entrainement et classification à partir des données sortantes d'Assembly Line par apprentissage supervisé*

[[_TOC_]]

# Principe
```mermaid
graph LR

Variables[Variables explicatives <br> x1, x2...xn] --> AI{"F(x1, x2...xn | p1, p2...pm)"}
Parameters[Hyperparamètres <br> p1, p2...pm] --> AI
AI --> Output[Variables à expliquer / target <br> y]
```

Notre premiere approche sera un apprentissage supervisé avec des données connues (*labeled data*). 
Ces données seront séparées aléatoirement en deux jeux de données distincts, 
un jeu de données pour l'entrainement et un jeu de données pour tester notre modele et valider notre entrainement.


<details>
<summary> data.csv</summary>
<table>
    <tr><td>metadata1</td><td>metadata2</td><td>class</td></tr>
    <tr><td>-7</td><td>14</td><td>0</td></tr>
    <tr><td>-72</td><td>52</td><td>0</td></tr>
    <tr><td>-63</td><td>53</td><td>0</td></tr>
    <tr><td>...</td><td>...</td><td>...</td></tr>
    <tr><td>68</td><td>76</td><td>1</td></tr>
    <tr><td>14</td><td>94</td><td>1</td></tr>
    <tr><td>61</td><td>91</td><td>1</td>
    </tr>
</table>
</details>


<details>
<summary> Architecture de code (lib scikit learning)</summary>

```python
import pandas as pd

from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn import model  # adapt

import pickle

from numpy import array

# x = variables, y = target

OUTPUT_VALUES = {
    0: 'Sain',
    1: 'Malware'
}
# data gathering
data = pd.read_csv('data.csv')
(entry_data, entry_class) = (data.iloc[:, :-1].values, data.iloc[:, -1].values)

# Labeled data handling
x_train, x_test, y_train, y_test = train_test_split(entry_data, entry_class, test_size=.2)
# Training
model.fit(x_train, y_train)
# validation : confusion matrice
confusion = confusion_matrix(y_test, model.predict(x_test))
print(confusion)
# validation : score
model.score(x_train, y_train)
# save
pickle.dump(model, open('model_pickle', 'wb'))
# reload & run 
model = pickle.load(open('scv_model_pickle', 'rb'))
input_data = [17, 18]
y_pred = model.predict(
    array([input_data])
)
print(OUTPUT_VALUES[y_pred[0]])  # ie print 'Malware' if y_pred == 1 else 'Sain'
```
</details>

Dans notre cas, nos variables explicatives seront les données sortantes d'assembly line (colonne 1 à n-1 nos métadata pour un fichier, et en colonne n la classe associée / target (malware ou sain)).
<br> Les hyperparametres sont notamment liés au modèle d'apprentissage (learning rate, number of layers, number of epochs...).
Nous pourrons nous plonger dans les mathematiques lorsque notre étude de cas sera aboutie et que le projet IA Malware BOLID sera plus avancé.
<br> Dans un premier temps, nous pouvons résumer nos *variables à expliquer* comme la qualification/categorisation d'un fichier selon la dichotomie suivante : { Malware | Sain }.
Nous utiliserons la lib matplotlib pour visualiser nos données. Dans notre démarche nous seront critique quant à la pertinence de nos données d'apprentissage, et nous nous refererons aux outils de validation de rigueur (matrice de confusion).
<br>
L'entrainement et l'utilisation de notre modele (quelqu'il soit) seront guidés par les étapes comme suit :
-   Collecte de données connues (labeled data)
-   Creation de nos matrices de données (X = [nb_sample, nb_feature] et Y = [nb_sample, nb_target])
    - *nb_target = 1 dans notre cas*
    - *nb_feature le nombre de paramètres qualifiants*
- Entrainement de notre model (*model.fit()*)
- Validation de notre model (*model.score()*, matrice de confusion avec le jeu de données de test préalablement isolé)
- Utilisation de notre modele sur une nouvelle donnée entrante *y_predict = model.predict(new_data)*

# Etude de cas
## Methode de travail - points clés 
- [ ] Modeliser
- [ ] Developper
- [ ] Securiser
- [ ] Tester
- [ ] Analyser
- [ ] Documenter
- [ ] Deployer

## Identifier le probleme
=> output assembly line 
=> entrainement avec des données connues
=> identification d'un malware
=> categorisation/classification selon les données de training
=> Quelles data ?? hash name size extension...

## Securité :
- [ ] Malicious input (fooling the system)
- [ ] Data poisoning
- [ ] Online system is still learning
- [ ] Popular model are easy to attack
- [ ] Data privacy / confidentiality


##  Les solutions
Les modeles (scikit learning):
  - Classification / Regression
  - Reconnaissance de motifs, systemes de recommandation, substitution de valeurs manquantes
    - [ ] SVM
      - Representation en N dimensions (nos N variables explicatives) de nos données d'entrainement
      - Recherche de l'hyperplan separateur optimal entre nos différents nuages de points
      - Capable de trouver des motifs non-linéaires grace a une hypersurface qui maximise la marge entre classes
      - Choix du kernel (regression lineaire, sigmoid...)
      - Pour une nouvelle entrée, le nouveau point est comparé aux nuages connus
    - [ ] KNN
      - Representation en N dimensions (nos N variables explicatives) de nos données d'entrainement
      - Pour une nouvelle entrée, analyse des k plus proches voisins et caracterisation
      - Attention à la pertinence et a la precision qui depend de k le nombre de voisins  
  - A etudier :
    - [ ] *random forest*
    - [ ] *Naive Bayes (text data)*
    - [ ] 
    - [ ] 
    
Supervised Learning :
- Linear regression (discovering thee connection between 2 persistent factors)
- Decision tree
- Naive Bayes Classification
- Logistic regression
- Ordinary Least Squares Regression

Unsupervised Learning :
- Clustering (K-means)

Algo :
- Linear regression
- Logistic regression
- Support Vector Machine (Large Margins Classifier)
- Naive Bayes (/!\ On suppose les variables independantes)
- Anomaly detection
- Decision Tree
- Neural Network
- K-means
- Gradient Descent

Reinforcement Learning :
- 


Les lib :
- [ ] scikit-learning
- [ ] numpy
- [ ] matplotlib
- [ ] pandas
- [ ] pickle
- [ ] *pytorch*
- [ ] *edward*
- [ ] *pyspark*
- [ ] 
- [ ] 


## Notes 
- OneHotEncoder pour traiter des data string (attention a l'usage de la memoire)
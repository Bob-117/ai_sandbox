import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import gridspec
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.svm import SVC
from tabulate import tabulate
from scipy.stats import gaussian_kde

logging.basicConfig(level=logging.ERROR,
                    format='\n#############\n%(message)s\n')

_log = logging.getLogger('main')

DATASET_SIZE = 500


# ## Dataset
def create_dataset():
    _size = DATASET_SIZE
    feature1 = np.random.randint(1, 100, size=_size)
    feature2 = np.random.rand(_size) * 8

    targets = np.where(
        np.logical_or(
            np.logical_and(feature1 < 20, feature2 > 7),
            np.logical_and(feature1 > 80, feature2 < 4),
        ),
        'classA', 'classB'
    )

    data = pd.DataFrame({'feature1': feature1, 'feature2': feature2, 'target': targets})
    data.to_csv('data.csv', index=False)


def load_dataset(_file):
    print(f'Chargement de {_file}')
    data = pd.read_csv(f'./{_file}')
    print_dataset(data)
    return data


def print_dataset(_df):
    print(tabulate(_df[0:min(5, DATASET_SIZE)], headers='keys', tablefmt='psql'))


# ## Plot
def plot(_data, _title):
    color_dict = {'classA': 'red', 'classB': 'blue'}

    color_series = _data['target'].map(color_dict)

    plt.scatter(_data['feature1'], _data['feature2'], c=color_series)

    plt.legend(
        handles=[
            plt.scatter([], [], c='red', label='classA'),
            plt.scatter([], [], c='blue', label='classB')
        ],
        bbox_to_anchor=(0, 0),
        loc="best",
        title='Target'
    )

    plt.title(_title)
    plt.xlabel('feature1')
    plt.ylabel('feature2')
    plt.legend()
    # plt.close()
    plt.savefig('all_2d')
    plt.show()


def plot2d(axes, _x, _y, _targets, _title):
    color_dict = {'classA': 'red', 'classB': 'blue'}
    color_series = _targets.map(color_dict)
    fig, ax = plt.subplots()
    ax.scatter(_x, _y, c=color_series)
    ax.set_title(_title)
    ax.set_xlabel(_x.name)
    ax.set_ylabel(_y.name)
    plt.show()


def plot_normalize(dataset, mode='bar'):
    #######################################################################
    # _axs = []
    # for norm in ['max_abs_scaling', 'min_max_feature_scaling', 'z_score', 'scikit_scaler']:
    #     _ax1 = dataset[f'{norm}_norm_feature1'].plot(kind='bar')
    #     plt.close()
    #     _ax2 = dataset[f'{norm}_norm_feature2'].plot(kind='bar')
    #     plt.close()
    #     _axs.append([norm, [_ax1, _ax2]])
    #
    # print(_axs)
    #######################################################################
    fig = plt.figure(figsize=(10, 8))
    outer = gridspec.GridSpec(2, 2, wspace=.5, hspace=1)

    for i, norm in enumerate(['max_abs_scaling', 'min_max_feature_scaling', 'z_score', 'scikit_scaler']):
        inner = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=outer[i], wspace=0.1, hspace=0.1)
        for j in range(2):
            ax = plt.Subplot(fig, inner[j])
            current_data = dataset[f'{norm}_norm_feature{j + 1}']
            if mode == 'bar':
                ax.bar(current_data.index, current_data.values)
                # title = '' + f'{norm}\n' if j % 2 == 0 else '' + f'feature{j + 1}'

            elif mode == 'density':
                density = gaussian_kde(current_data)
                xs = np.linspace(0, max(current_data))
                density.covariance_factor = lambda: .25
                density._compute_covariance()
                ax.plot(xs, density(xs))

            ax.legend([f'feature{j + 1}'])
            if j % 2 == 0:
                ax.set_title(norm)
            fig.add_subplot(ax)

    #######################################################################
    # fig = plt.figure(figsize=(10, 8))
    # outer = gridspec.GridSpec(2, 2, wspace=0.2, hspace=0.2)
    #
    # for i in range(4):
    #     inner = gridspec.GridSpecFromSubplotSpec(2, 1,
    #                                              subplot_spec=outer[i], wspace=0.1, hspace=0.1)
    #
    #     for j in range(2):
    #         ax = plt.Subplot(fig, inner[j])
    #         t = ax.text(0.5, 0.5, 'outer=%d, inner=%d' % (i, j))
    #         t.set_ha('center')
    #         ax.set_xticks([])
    #         ax.set_yticks([])
    #         fig.add_subplot(ax)
    #######################################################################

    # plt.savefig(f'{mode}')
    plt.show()


# ## NaN
def identify_nan_values(_df):
    nan_values = _df.isna().sum()
    print(f"Nombre de <NaN> pour chaque colonne:\n{nan_values}")
    nan_col = list(
        filter(
            lambda key: nan_values[key] > 0,
            nan_values.keys().tolist()
        )
    )
    print(f'Colonnes présentant un ou des <NaN> : {nan_col}')
    return nan_col


def identify_nan_in_specific_column(_df, _col):
    # TODO use this
    """
    for c in identify_nan_values(_df):
        identify_nan_in_specific_column(c)

    yield idx & gg
    """
    _ids = _df[_df[_col].isnull()].index.tolist()
    return _ids


def replace_nan_by_mean_in_specific_column(_df, col_name):
    col_mean = _df[col_name].mean()

    print(f'Remplacement des <NaN> dans <{col_name}> par la moyenne de la colonne ( {col_mean} )')

    _df[col_name] = _df[col_name].fillna(col_mean)
    print_dataset(_df)
    return _df


def remove_nan_rows(_df, col_name):
    print(f'Suppression des lignes avec un <NaN> dans la colonne <{col_name}>')
    _df = _df.dropna(subset=[col_name])
    print_dataset(_df)
    return _df


def nan_process(_df, _nan_col):
    for col in _nan_col:
        if col == 'feature1':
            _df = replace_nan_by_mean_in_specific_column(_df=_df, col_name=col)
        elif col == 'feature2':
            _df = remove_nan_rows(_df=_df, col_name=col)
        else:
            _log.warning('nope')
    return _df.copy()


# ## Outliers
def identify_outliers(_df):
    _outliers = []
    for col in _df.columns:
        if _df[col].dtype != 'object':
            mean = _df[col].mean()
            std = _df[col].std()
            _outliers.extend(_df[(_df[col] < mean - 3 * std) | (_df[col] > mean + 3 * std)].index.tolist())
    print(f"Nombre d'outliers identifiés: {len(_outliers)}")
    print(f"Indices des outliers: {_outliers}")
    return list(set(_outliers))


def remove_outliers():
    ...


# ## Normalize
def normalize(_df, included_col, excluded_col):
    """
    import pandas as pd
    df = pd.DataFrame({
                   'A':[1,2,3],
                   'B':[100,300,500],
                   'C':list('abc')
                 })
    print(df)
       A    B  C
    0  1  100  a
    1  2  300  b
    2  3  500  c

    WITH PANDAS
    df.iloc[:,0:-1] = df.iloc[:,0:-1].apply(lambda x: (x-x.mean())/ x.std(), axis=0)
    print(df)
         A    B  C
    0 -1.0 -1.0  a
    1  0.0  0.0  b
    2  1.0  1.0  c

    WITH SCIKIT
    from sklearn.preprocessing import StandardScaler
    scaler = StandardScaler()

    df.iloc[:,0:-1] = scaler.fit_transform(df.iloc[:,0:-1].to_numpy())
    print(df)
              A         B  C
    0 -1.224745 -1.224745  a
    1  0.000000  0.000000  b
    2  1.224745  1.224745  c

    We use a biased estimator for the standard deviation, equivalent to numpy.std(x, ddof=0).
    Note that the choice of ddof is unlikely to affect model performance.
    https://stackoverflow.com/questions/26414913/normalize-columns-of-a-dataframe
    """
    print(f'Normalisation des colonnes {included_col}, exclusion des colonnes {excluded_col}')

    all_df = _df.copy()

    for c in included_col:
        """
        The maximum absolute scaling rescales each feature between -1 and 1 
        by dividing every observation by its maximum absolute value. 
        We can apply the maximum absolute scaling in Pandas using the .max() and .abs() methods, as shown below.
        """
        all_df[f'max_abs_scaling_norm_{c}'] = all_df[c] / all_df[c].abs().max()

        """
        The min-max approach (often called normalization) rescales the feature to a hard and fast range of [0,1] 
        by subtracting the minimum value of the feature then dividing by the range. 
        We can apply the min-max scaling in Pandas using the .min() and .max() methods.
        """
        all_df[f'min_max_feature_scaling_norm_{c}'] = (all_df[c] - all_df[c].min()) / (
                all_df[c].max() - all_df[c].min())

        """
        The z-score method (often called standardization) transforms the info into distribution 
        with a mean of 0 and a typical deviation of 1. 
        Each standardized value is computed by subtracting the mean 
        of the corresponding feature then dividing by the quality deviation.
        """
        all_df[f'z_score_norm_{c}'] = (all_df[c] - all_df[c].mean()) / all_df[c].std()

        """
        Transform features by scaling each feature to a given range. 
        This estimator scales and translates each feature individually 
        such that it is in the given range on the training set, e.g. between zero and one. 
        Here, we will use minmax scaler.
        """
        all_df[f'scikit_scaler_norm_{c}'] = MinMaxScaler().fit_transform(np.array(all_df[c]).reshape(-1, 1))

    # print_dataset(all_df)
    return all_df


def corrupted(dataset):
    # ## OOF
    # dataset.loc[:, ('feature1', 0)] = 117
    # dataset[dataset.columns['feature1'][0]] = 117
    # dataset.__getitem__('feature1').__setitem__(0, 117)
    # dataset['column name'] = dataset['column name'].replace(['old value'], 'new value')
    # dataset.ix['feature1', 0] = 118
    print('\U0001f52b')
    dataset.loc[0:1, 'feature1'] = np.nan
    dataset.loc[1:1, 'feature2'] = np.nan
    dataset.loc[3:3, 'feature2'] = 2000
    print_dataset(dataset)
    return dataset


def main_process():
    create_dataset()
    dataset = load_dataset(_file='data.csv')
    dataset = corrupted(dataset)  # <== /!\
    plot2d(axes=[], _x=dataset['feature1'], _y=dataset['feature2'], _targets=dataset['target'],
           _title='plot 2d f1 over f2')
    plot(dataset, _title='plot f1 over f2')
    nan_col = identify_nan_values(_df=dataset)
    dataset = nan_process(_df=dataset, _nan_col=nan_col)

    # do something with outlier, remove, logarithm..
    outliers_ids = identify_outliers(dataset)
    dataset.drop(outliers_ids[0], inplace=True)  # <== /!\

    print_dataset(dataset)

    dataset = normalize(_df=dataset, included_col=['feature1', 'feature2'], excluded_col=['target'])
    print('normalized dataset info :')
    dataset.info()
    dataset.to_csv('final.csv', index=False)
    plot(dataset, _title='plot normalized')

    print('plotting bar mode')
    plot_normalize(dataset, mode='bar')
    print('plotting density mode')
    plot_normalize(dataset, mode='density')


def validation():
    print(
        'accuracy = (TN + TP) / (TN + FP + FN + TP)'
        'balanced accuracy = (sensitivity + specificity) / 2 arithmetic mean'
        'sensitivity = TP / (TP + FN)'
        '(sensibilité élevée  = peu de faux positifs)'
        'specificity = TN / (TN + FP)'
        '(spécificité élevée  = peu de faux négatifs)'
        'f1 score = 2 * ((precision*recall)/(precision + recall))'
        'fβ score = (1 + β^2) * ( (precision*recall) / ( (β^2 * precision) + recall ) )'
        'choix β comme on veux, β<1 aide a la precision, β>1 aide recall, β=1 f1score (du coup)'
        'Matthews Correlation Coefficient'
        'MCC = (TP * TN) - (FP * FN) / sqrt ( (TP+FP) * (TP+FN) * (TN+FP) * (TN+FN) )'
        'je calcule tout ca, en je CHOISIS ma métrique en fonction de ma conf mat idéale ?'

        'ROC on probas, not predicted class'

        'SMOTE (Synthetic Minority Over-Sampling TEchnique) '
    )


def ai_process():
    print('\n\n=======> AI ROC PROCESS')
    from sklearn.model_selection import train_test_split
    from sklearn.preprocessing import StandardScaler
    from sklearn.svm import SVC
    from sklearn.neighbors import KNeighborsClassifier
    from sklearn.metrics import roc_curve, roc_auc_score
    import matplotlib.pyplot as plt

    data = pd.read_csv("data.csv", usecols=['feature1', 'feature2', 'target'])
    # print(data)
    _features = data[['feature1', 'feature2']]
    _targets = data['target'].map({'classA': 0, 'classB': 1})

    scaler = StandardScaler()
    _features = scaler.fit_transform(_features)

    x_train, x_test, y_train, y_test = train_test_split(_features, _targets, test_size=0.2)

    # diag

    plt.subplot(211)
    plt.plot([0, 1], [0, 1], 'k--')
    # SVM
    svm = SVC(kernel='linear', probability=True)
    svm.fit(x_train, y_train)

    # WE USE PROBS HERE !!!!!
    svm_probs = svm.predict_proba(x_test)[:, 1]
    # print(svm_probs)

    # ROC  SVM
    fpr_svm, tpr_svm, _ = roc_curve(y_test, svm_probs)
    auc_svm = roc_auc_score(y_test, svm_probs)
    plt.plot(fpr_svm, tpr_svm, label=f'SVM (AUC = {auc_svm:.2f})')

    # KNN1
    knn = KNeighborsClassifier(n_neighbors=2)
    knn.fit(x_train, y_train)

    # WE USE PROBS HERE !!!!!
    knn_probs = knn.predict_proba(x_test)[:, 1]

    # ROC  KNN1
    fpr_knn, tpr_knn, _ = roc_curve(y_test, knn_probs)
    auc_knn = roc_auc_score(y_test, knn_probs)
    plt.plot(fpr_knn, tpr_knn, label=f'KNN 2 (AUC = {auc_knn:.2f})')

    # KNN2
    knn = KNeighborsClassifier(n_neighbors=8)
    knn.fit(x_train, y_train)

    # WE USE PROBS HERE !!!!!
    knn_probs = knn.predict_proba(x_test)[:, 1]

    # ROC  KNN2
    fpr_knn, tpr_knn, _ = roc_curve(y_test, knn_probs)
    auc_knn = roc_auc_score(y_test, knn_probs)
    plt.plot(fpr_knn, tpr_knn, label=f'KNN 8 (AUC = {auc_knn:.2f})')

    # plot
    plt.xlabel('Taux de faux positifs')
    plt.ylabel('Taux de vrais positifs')
    plt.title('Courbe ROC pour SVM et KNN')
    plt.legend(loc='lower right')

    plt.subplot(212)
    _infos = '\n'.join((
        f'- donne le taux de vrais positifs (fraction des positifs qui sont effectivement détectés) \n'
        f'en fonction du taux de faux positifs (fraction des négatifs qui sont incorrectement détectés).\n',
        f'Si le modèle calcule un score s qui est comparé au seuil S pour prédire la classe',
        f'([s ≥ S] = positif et [s < S] = négatif, généralement), et qu’on compare ensuite avec les classes positif ',
        f'et négatif réelles, la sensibilité est donnée par le taux de positifs réels classés positifs, ',
        f'et l’antispécificité (1 moins la spécificité) par le taux de négatifs réels classés positifs. On met ',
        f'l’antispécificité en abscisse et la sensibilité en ordonnée pour former la courbe ROC. Chaque valeur de S ',
        f'fournit un point de la courbe, qui croit (non-strictement) de (0, 0) à (1, 1). ',
        'En (0, 0) le classificateur classe tout négatif : il n’y a aucun faux positif, mais également aucun vrai ',
        'positif. ',
        'Les proportions de vrais et faux négatifs dépendent de la population sous-jacente. ',
        'En (1, 1) le classificateur classe tout positif : il n’y a aucun vrai négatif, mais également aucun faux ',
        'négatif. Les proportions de vrais et faux positifs dépendent de la population sous-jacente. ',
        'Un classificateur aléatoire tracera une droite allant de (0, 0) à (1, 1).',
        'En (0, 1) le classificateur n’a aucun faux positif ni aucun faux négatif, et est par conséquent parfaitement ',
        'exact, ne se trompant jamais. ',
        'En (1, 0) le classificateur n’a aucun vrai négatif ni aucun vrai positif, et est par conséquent parfaitement ',
        'inexact, se trompant toujours. Il suffit d’inverser sa prédiction pour en faire un classificateur ',
        'parfaitement exact. ',
        'Attention, on calcule sur proba'
    ))
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    plt.text(0, 1, _infos, transform=plt.subplot(212).transAxes, fontsize=8,
             verticalalignment='top', bbox=props)
    plt.axis('off')
    plt.show()


def demo_roc():
    models = [
        SVC(kernel='linear', probability=True),
        SVC(kernel='sigmoid', probability=True),
        KNeighborsClassifier(n_neighbors=3),
        KNeighborsClassifier(n_neighbors=8),
        RandomForestClassifier(
            max_features=8,
            max_depth=9,
            ccp_alpha=0,
            random_state=1001
        )

    ]

    data = pd.read_csv("data.csv", usecols=['feature1', 'feature2', 'target'])
    # print(data)
    _features = data[['feature1', 'feature2']]
    _targets = data['target'].map({'classA': 0, 'classB': 1})

    scaler = StandardScaler()
    _features = scaler.fit_transform(_features)

    x_train, x_test, y_train, y_test = train_test_split(_features, _targets, test_size=0.2)


    fig,ax = plt.subplots()
    ax.plot([0, 1], [0, 1], 'k--')
    for model in models:
        model.fit(x_train, y_train)

        # WE USE PROBS HERE !!!!!
        probs = model.predict_proba(x_test)[:, 1]
        # print(svm_probs)

        # ROC  SVM
        fpr_svm, tpr_svm, _ = roc_curve(y_test, probs)
        auc_svm = roc_auc_score(y_test, probs)
        ax.plot(fpr_svm, tpr_svm, label=f'{model}')

    fig.add_subplot(ax)
    # plt.xlabel('Taux de faux positifs')
    # plt.ylabel('Taux de vrais positifs')
    # plt.title('Courbe ROC pour SVM et KNN')
    plt.legend(loc='lower right')
    plt.show()

def decision_tree():
    import matplotlib.pyplot as plt
    import numpy as np
    from sklearn.metrics import roc_auc_score, balanced_accuracy_score, matthews_corrcoef
    from sklearn.model_selection import RandomizedSearchCV
    from sklearn import tree
    print('\n\n=======> DECISION TREE PROCESS')

    # split
    # x_train = y_train = x_test = y_test = []
    data = pd.read_csv("final.csv", usecols=['feature1', 'feature2', 'target'])
    # print(data)
    _features = data[['feature1', 'feature2']]
    _targets = data['target'].map({'classA': 0, 'classB': 1})

    scaler = StandardScaler()
    _features = scaler.fit_transform(_features)

    x_train, x_test, y_train, y_test = train_test_split(_features, _targets, test_size=0.2)
    _param_distributions = {
        'max_features': range(4, 9),
        'max_depth': range(5, 10)
    }

    search = RandomizedSearchCV(
        estimator=tree.DecisionTreeClassifier(random_state=1001),
        n_iter=5,
        param_distributions=_param_distributions,
        random_state=0
    )

    search.fit(x_train, y_train)
    a = search.best_params_
    print(f'Best params : {a}')
    search.score(x_test, y_test)

    preds = search.predict(x_test)

    balanced_accuracy_score(y_test, preds)
    matthews_corrcoef(y_test, preds)
    roc_auc_score(y_test, preds)

    _decision_tree = tree.DecisionTreeClassifier(max_features=8, max_depth=8)

    _decision_tree = _decision_tree.fit(x_train, y_train)

    plt.figure()
    tree.plot_tree(_decision_tree, filled=True)
    # plt.savefig('decision_tree_estimator')
    plt.show()


def random_forest():
    import matplotlib.pyplot as plt
    import numpy as np
    from sklearn.metrics import roc_auc_score, balanced_accuracy_score, matthews_corrcoef
    from sklearn.model_selection import RandomizedSearchCV
    from sklearn.ensemble import RandomForestClassifier

    print('\n\n=======> RANDOM FOREST PROCESS')

    # split
    x_train = y_train = x_test = y_test = []
    data = pd.read_csv("final.csv", usecols=['feature1', 'feature2', 'target'])
    _features = data[['feature1', 'feature2']]
    _targets = data['target'].map({'classA': 0, 'classB': 1})

    scaler = StandardScaler()
    _features = scaler.fit_transform(_features)

    x_train, x_test, y_train, y_test = train_test_split(_features, _targets, test_size=0.2)

    _param_distributions = {
        'max_features': range(5, 10),
        'max_depth': range(5, 10),
        'ccp_alpha': range(0, 1)
    }

    search = RandomizedSearchCV(
        estimator=RandomForestClassifier(random_state=1001),
        n_iter=5,
        param_distributions=_param_distributions,
        random_state=1001,
    )

    search.fit(x_train, y_train)

    a = search.best_params_
    print(f'Best params for random forest: {a}')
    search.score(x_test, y_test)

    preds = search.predict(x_test)

    balanced_accuracy_score(y_test, preds)
    matthews_corrcoef(y_test, preds)
    roc_auc_score(y_test, preds)

    forest = RandomForestClassifier(
        max_features=8,
        max_depth=9,
        ccp_alpha=0,
        random_state=1001
    )
    forest.fit(x_train, y_train)

    importances = forest.feature_importances_

    std = np.std(
        [tree.feature_importances_ for tree in forest.estimators_],
        axis=0
    )

    forest_importances = pd.Series(importances, index=['A', 'B'])
    # TODO get features name
    # print(forest_importances)
    # print(preds)
    fig, ax = plt.subplots()
    forest_importances.plot.bar(yerr=std, ax=ax)
    fig.tight_layout()
    # plt.savefig('random_forest_estimator')
    plt.show()


def demo_process():
    data = pd.read_csv("final.csv", usecols=['feature1', 'feature2', 'target'])
    _features = data[['feature1', 'feature2']]
    _targets = data['target'].map({'classA': 0, 'classB': 1})
    fig, ax = plt.subplots()
    current_data = data['feature2']

    ax.bar(current_data.index, current_data.values)
    fig.add_subplot(ax)
    plt.show()

    fig, ax = plt.subplots()
    density = gaussian_kde(current_data)
    xs = np.linspace(0, max(current_data))
    density.covariance_factor = lambda: .25
    density._compute_covariance()
    ax.plot(xs, density(xs) * 100, c='red')
    fig.add_subplot(ax)
    plt.show()


# main_process()
# decision_tree()
# random_forest()
# ai_process()

demo_roc()

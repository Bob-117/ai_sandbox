from abc import ABC

import numpy as np
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
from sklearn.model_selection import train_test_split


class AIModel(ABC):

    def __init__(self, _model, _data):
        # initial training dataset & classifier
        self.dataset = _data
        self.classifier = _model
        self.features_name = []
        self.target_name = []
        # training data
        self.__data = None
        self.__features = None
        self.__target = None
        self.__feature_train = None
        self.__feature_test = None
        self.__target_train = None
        self.__target_test = None
        # validation
        self.__confusion = None
        self.__accuracy = 0
        self.__recall = 0
        self.__precision = 0
        self.__f1_score = 0
        self.__roc = None

    # ## TRAIN CLASSIFIER ##############
    """
    Please note that the input data must be processed.
    Otherwise it probably won't work :^)
    """

    def __split_data(self, test_size=.2, data_in=None):
        """
        split dataset between :
        - feature_train ie x_train
        - target_train ie y_train
        - feature_test ie x_test
        - target_train ie y_test
        """
        # data = pd.DataFrame(data.data, columns=data.feature_names)
        self.features_list = data_in.feature_names
        (self.__features, self.__target) = data_in.data, data_in.target.reshape(len(data_in.target), 1)

        _entry_feature, _entry_target = self.__features, self.__target
        self.__feature_train, self.__feature_test, self.__target_train, self.__target_test = train_test_split(
            _entry_feature, _entry_target, test_size=test_size
        )
        return [self.__feature_train, self.__feature_test, self.__target_train, self.__target_test]

    def __custom_split_data(self, test_size=.2):
        raise NotImplementedError

    def train(self):
        """
        split data (x_train, y_train, x_test, y_test)
        train model with classifier.fit(x_train, y_train)
        display 2d or 3d graph depending on features number (x)
        :return:
        """
        # self.__load_data_from_csv()
        # self.__display_data()

        self.__split_data(data_in=self.dataset)
        self.classifier.fit(self.__feature_train, self.__target_train.reshape(len(self.__target_train, )))
        try:
            score = self.classifier.score(self.__feature_test, self.__target_test)
            score_msg = f'{score}, {"Success training" if score > .8 else "Failed training => need more/better data"}'
        except AttributeError:
            score_msg = 'not implemented score'
        print(f'Score : {score_msg}')
        self.__compute_metrics()

        # self.plot_confusion_matrix()
        # self.__generate_graph()
        return self.classifier

    # ## MANAGE CLASSIFIER ##############

    def __save(self):
        """
        Save a trained model
        :return:
        """
        # assert self.classifier.trained ?
        raise NotImplementedError('pickle.dump()')

    def __reload_model(self):
        """
        Reload a trained model (with metrics ? how to store?)
        :return:
        """
        raise NotImplementedError('pickle.load()')

    # ## VALIDATE CLASSIFIER ##############

    def __confusion_matrix(self):
        """
        Scikit Learning confusion mtx
        :return:
        """
        self.__confusion = confusion_matrix(self.__target_test, self.classifier.predict(self.__feature_test))
        return self.__confusion

    def __custom_confusion(self):
        """
        True Positive TP
        False Positive FP
        True Negative TN
        False Negative FN

        Positive = TP + FN
        Negative = TN + FP

        TP_rate = TP / (TP + FN) = TP / Positive
        FP_rate = FP / (FP + TN) = FP / Negative
        TN_rate = TN / (TN + FP) = TN / Negative
        FN_rate = FN / (FN + TP) = FN / Positive

        good classifier = high TP & TN
        [[10, 1],
        [1, 10]]
        good classifier = high TP_rate, high TN_rate, low FP_rate & low FN_rate
        """
        if len(np.unique(self.__target)) == 2:
            # Noob way
            TP = FP = TN = FN = 0
            for y, y_hat in zip(self.__target_test, self.classifier.predict(self.__feature_test)):
                if y == 1 and y_hat == 1:
                    TP += 1
                elif y == 0 and y_hat == 0:
                    TN += 1
                elif y == 1 and y_hat == 0:
                    FN += 1
                elif y == 0 and y_hat == 1:
                    FP += 1
            return np.array([
                [TN, FP],
                [FN, TP]
            ])
        else:
            # TP += int(y == 1 and y_hat == 1)
            # TN += int(y == 0 and y_hat == 0)
            # FN += int(y == 1 and y_hat == 0)
            # FP += int(y == 0 and y_hat == 1)

            #  y      y_pred
            # [0]      [0]
            # [0]      [1]
            # [0]      [0]          [ 2  1  0 ]
            # [1]      [1]     =>   [ 0  2  0 ]
            # [1]      [1]          [ 0  1  1 ]
            # [2]      [2]
            # [2]      [1]

            raise NotImplementedError('Generalize conf mtx for n > 2 targets')

    def __compute_metrics(self):
        """
        # TODO from scratch
        :return:
        """
        self.__accuracy = accuracy_score(
            self.__target_test,
            self.classifier.predict(self.__feature_test)
        )
        self.__precision = precision_score(
            self.__target_test,
            self.classifier.predict(self.__feature_test),
            average='weighted'
        )
        self.__recall = recall_score(
            self.__target_test,
            self.classifier.predict(self.__feature_test),
            average='weighted'
        )
        self.__f1_score = f1_score(
            self.__target_test,
            self.classifier.predict(self.__feature_test),
            average='weighted'
        )

    def precision(self):
        return precision_score(self.__target_test, self.classifier.predict(self.__feature_test), average='weighted')

    def precision_recall_score(self):
        """
        Precision = 0 < TP / (TP + FP) < 1
        for all points declared positive, what percentage are actually positive

        Recall = 0 < TP / (TP + FN) = TP / Positive < 1
        for all points actually positive, what percentage was the model able to detect/predict
        """
        pass

    def f1_score(self):
        """
        f1_score = 0 < 2 * (precision * recall) / (precision + recall) < 1
        higher = better
        :return:
        """
        pass

    # ## PREDICT WITH A TRAINED CLASSIFIER ##############

    def predict(self, data):
        """
        target_predict = model.predict(data) = [0] | [1]
        based on previous training
        :param data:
        :return:
        """
        return self.classifier.predict(
            np.array([data])
        )

    def predict_process(self, data):
        """
        Run the IA prediction on a new data entry
        :param data:
        :return:
        """
        print(f'-----RUNNING PREDICTION for {data} '
              f'based on {self.dataset} '
              f'training (model : {self.classifier})-------')
        result = self.predict(data)
        print(f'We estimate {data} to be target {result[0]} with {self.precision()}e2%')
        return result[0]

    # ## __________________ ##############

    def huho(self):
        # print(f'-----TRAINING RESULTS for {self.dataset}-------')
        print(f'-----TRAINING RESULTS-------')
        print(
            f'nb_sample: {len(self.__features)} | {len(self.__target)}, train : {len(self.__feature_train)} samples, test : {len(self.__feature_test)} samples')
        print(f'conf mtx:{self.__confusion_matrix()}')
        print(f"Accuracy: {self.__accuracy}")
        print(f"Precision: {self.__precision}")
        print(f"Recall: {self.__recall}")
        print(f"F1-score: {self.__f1_score}")

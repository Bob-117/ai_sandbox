import sklearn
from sklearn.datasets import load_iris
from sklearn.svm import SVC
from custom_svm import CustomSVM
from svm_from_scratch.ai_model import AIModel


def process():
    scikit_svm = SVC()
    custom_svm = CustomSVM()

    models = [scikit_svm, custom_svm]
    data = load_iris()

    for model in models:
        svm_process(data=data, model=model)


def svm_process(data, model):
    print(f'DATA SVM PROCESS {len(data)}')
    m = AIModel(_data=data, _model=model)

    print(f'Model : {model} - Train')
    m.train()
    print(f'Model : {model} - Predict')
    m.predict([0, 2, 5, 6])

    m.huho()


process()

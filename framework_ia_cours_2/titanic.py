import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn.neighbors import KNeighborsClassifier

pd.options.display.float_format = '{:.2f}'.format

dataset = pd.read_csv('titanic.csv')

print(dataset)
print(dataset.head())
print(dataset.shape)
print(dataset.columns)
print(dataset.describe())
print(dataset.info)
print(dataset.isnull().sum())
sns.heatmap(dataset.isnull(), cmap='magma', cbar=False)
plt.show()

# sur = train['Survived'].value_counts()
# survival_rate = [sur[0]/len(train)*100,sur[1]/len(train)*100]


dataset = pd.read_csv('titanic.csv', converters={'Sex': lambda sex: sex == 'male'})
x = dataset[['Sex', 'Age']]
y = dataset['Survived']
a = x['Age'].median()
print(a)
x['Age'] = x['Age'].fillna(a)


for k in range(1, 6):
    model = KNeighborsClassifier(n_neighbors=k)
    model.fit(x.values, y.values)


    person = [[1, 24]]
    model.predict(person)


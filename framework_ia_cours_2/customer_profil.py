from abc import ABC

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn import datasets
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.hierarchy as sch
from sklearn.model_selection import train_test_split


class AIModel(ABC):

    def __init__(self, **kwargs):
        # initial training dataset & classifier
        self.data = kwargs.get('data') or None
        self.file = kwargs.get('file') or None
        self.classifier = None
        self.features_list = []
        # training data
        self.variable = None
        self.target = None
        self.var_train = None
        self.var_test = None
        self.target_train = None
        self.target_test = None
        # validation
        self.confusion = None
        self.recall = None
        self.f1_score = None

    def load_data_from_file(self):
        """
        load dataset from csv file
        :return:
        """
        data = pd.read_csv(self.file)
        self.features_list = list(data.keys())
        (variable, target) = (data.iloc[:, :-1].values, data.iloc[:, -1].values)
        self.variable, self.target = variable, target.reshape(len(target), 1)  # always handle m*n array
        return self.variable, self.target

    def load_data_from_scikit_datasets(self):
        """
        load dataset from default scikit datasets
        :return:
        """
        iris = datasets.load_iris()
        # data = pd.DataFrame(data.data, columns=data.feature_names)
        self.features_list = iris.feature_names
        (self.variable, self.target) = iris.data, iris.target.reshape(len(iris.target), 1)
        return self.variable, self.target

    def load_data(self):
        """
        load data from file.csv or from scikit datasets
        file has priority
        """
        if self.file:
            return self.load_data_from_file()
        elif self.data:
            return self.load_data_from_scikit_datasets()
        else:
            self.load_default_data()
            # data = datasets.load_iris()
            # data = pd.DataFrame(data.data, columns=data.feature_names)

    def load_default_data(self):
        """
        useless anyway
        :return:
        """
        if self.data == 'iris':
            default_data = datasets.load_iris()
        elif self.data == 'wine':
            default_data = datasets.load_wine()
        else:
            default_data = datasets.load_digits()
        # return default_data
        return self.load_data_from_scikit_datasets()

    def split_data(self, test_size=.2):
        """
        split dataset between :
        - variable_train ie x_train
        - target_train ie y_train
        - variable_test ie x_test
        - target_train ie y_test
        """
        entry_var, entry_target = self.load_data()
        self.var_train, self.var_test, self.target_train, self.target_test = train_test_split(
            entry_var, entry_target, test_size=test_size
        )
        return [self.var_train, self.var_test, self.target_train, self.target_test]


# model = AIModel(file='hierarchical-clustering-with-python-and-scikit-learn-shopping-data.csv')
# model.load_data_from_file()
# print(model.target)

dataset = pd.read_csv('hierarchical-clustering-with-python-and-scikit-learn-shopping-data.csv')

X = dataset.iloc[:, [3, 4]].values
dendrogram = sch.dendrogram(sch.linkage(X, method='ward'))
plt.show()

colors = {
    1: 'red',
    2: 'blue',
    3: 'green',
    4: 'purple',
    5: 'orange'
}

fig, ax = plt.subplots(nrows=2, ncols=3)

for i in range(6):

    model = AgglomerativeClustering(n_clusters=i+1, affinity='euclidean', linkage='ward')
    model.fit(X)
    labels = model.labels_
    ax[i // 3][i % 3].scatter(X[labels == 0, 0], X[labels == 0, 1], s=50, marker='o', color='red')
    ax[i // 3][i % 3].scatter(X[labels == 1, 0], X[labels == 1, 1], s=50, marker='o', color='blue')
    ax[i // 3][i % 3].scatter(X[labels == 2, 0], X[labels == 2, 1], s=50, marker='o', color='green')
    ax[i // 3][i % 3].scatter(X[labels == 3, 0], X[labels == 3, 1], s=50, marker='o', color='purple')
    ax[i // 3][i % 3].scatter(X[labels == 4, 0], X[labels == 4, 1], s=50, marker='o', color='orange')
    ax[i // 3][i % 3].title.set_text(f'nb cluster {i + 1}')
plt.show()
# print(labels)
# fig = plt.figure()
# plt.scatter(X[labels == 0, 0], X[labels == 0, 1], s=50, marker='o', color='red')
# plt.scatter(X[labels == 1, 0], X[labels == 1, 1], s=50, marker='o', color='blue')
# plt.scatter(X[labels == 2, 0], X[labels == 2, 1], s=50, marker='o', color='green')
# plt.scatter(X[labels == 3, 0], X[labels == 3, 1], s=50, marker='o', color='purple')
# plt.scatter(X[labels == 4, 0], X[labels == 4, 1], s=50, marker='o', color='orange')
# plt.show()


print(dataset.head())
print(dataset.shape)

k = 'entrez le nb de cluster'
k = 5

data = dataset.iloc[:, 1:5].values
genre = data[:, 0]
print(type(genre))
age = data[:, 1]
income = data[:, 2]
spending_score = data[:, 3]

cluster = AgglomerativeClustering(n_clusters=k).fit_predict(data)

plt.figure(1)
plt.scatter(income, spending_score, c=cluster)
plt.xlabel('revenu annuel')
plt.ylabel('spending score')
plt.show()

from sklearn.cluster import kmeans_plusplus
from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt

num_samples = 2400
cluster_centers = [(3, 3), (6, 9), (10, 5)]
num_classes = len(cluster_centers)

X, y = make_blobs(n_samples=num_samples, centers=cluster_centers, n_features=num_classes, center_box=(0, 1),
                  cluster_std=0.6, random_state=0)

centers_init, indices = kmeans_plusplus(X, n_clusters=num_classes, random_state=0)

plt.figure(1)
colors = ["#4EACC5", "#FF9C34", "#4E9A06"]

for k, col in enumerate(colors):
    cluster_data = y == k
    plt.scatter(X[cluster_data, 0], X[cluster_data, 1], c=col, marker=".", s=10)

plt.scatter(centers_init[:, 0], centers_init[:, 1], c="b", s=50)
plt.title("Présentation de l'algorithme K-Means++")
plt.show()

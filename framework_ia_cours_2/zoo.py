import pandas as pd
from matplotlib import pyplot as plt

from sklearn.metrics import roc_curve
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.model_selection import train_test_split

zoo = pd.read_csv('zoo.csv')

print(zoo.describe())
print(zoo)
zoo.drop("animal_name",axis=1,inplace=True)
print(zoo)

# ##
# ##
# ##
# ##

# hair = 1 no = 0
x, y = zoo.loc[:, (zoo.columns != 'hair')], zoo.loc[:, 'hair']
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=42)
logreg = LogisticRegression()
logreg.fit(x_train, y_train)
y_pred_prob = logreg.predict_proba(x_test)[:, 1]
fpr, tpr, thresholds = roc_curve(y_test, y_pred_prob)
# Plot ROC curve
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(fpr, tpr)
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC')
plt.show()

from collections import Counter
import matplotlib.pyplot as plt


class Graph:
    colors = {
        0: 'blue',
        1: 'green',
        2: 'yellow',
        3: 'purple',
        4: 'pink'
    }

    @staticmethod
    def display_2d_graph(var, target):
        """
        display our dataset
        useful to identify outlier data
        :return:
        """
        nb_sample = len(var)
        current_size = [0, 0, 0, 0]
        for var, target in zip(var, target):
            current_size = Graph.size_training_2d_graph(current_size, var[0], var[1])
            plt.scatter(var[0], var[1], color=Graph.colors.get(target[0], 'black'), zorder=1)
        plt.axis(list(map(Graph.round_hundred, current_size)))
        plt.title(f"TRAINING with {nb_sample} samples")
        return plt

    @staticmethod
    def display_3d_graph(var, target, axes):
        """
        display our dataset
        useful to identify outlier data
        :return:
        """

        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')
        ax.set_xlabel(axes[0])
        ax.set_ylabel(axes[1])
        ax.set_zlabel(axes[2])

        current_size = [0, 0, 0, 0, 0, 0]
        for var, target in zip(var, target):
            # current_size = Graph.size_training_3d_graph(current_size, var[0], var[1], var[2])
            ax.scatter(var[0], var[1], var[2], color=Graph.colors.get(target[0], 'black'), label=target[0], zorder=1)
        # labels = sorted([f'{item}: {count}' for item, count in Counter(target).items()])
        # ax.legend(labels, loc='lower right', title='legend')
        # plt.axis(list(map(Graph.round_hundred, current_size)))
        # ax.legend(['a', 'b', 'c'], Graph.colors)
        plt.title("TRAINING")

        return plt

    @staticmethod
    def size_training_2d_graph(current_size, width, height):
        """
        resize graph to bbe sure we display all points
        useful to identify outlier data
        :param current_size:
        :param width:
        :param height:
        :return:
        """
        min_w = current_size[0]
        max_w = current_size[1]
        min_h = current_size[2]
        max_h = current_size[3]
        if width < min_w:
            min_w = width
        if width > max_w:
            max_w = width
        if height < min_h:
            min_h = height
        if height > max_h:
            max_h = height
        return [min_w, max_w, min_h, max_h]

    @staticmethod
    def size_training_3d_graph(current_size, width, height, depth):
        """
        resize graph to bbe sure we display all points
        useful to identify outlier data
        :param current_size:
        :param width:
        :param height:
        :param depth:
        :return:
        """
        min_x = current_size[0]
        max_x = current_size[1]
        min_y = current_size[2]
        max_y = current_size[3]
        min_z = current_size[4]
        max_z = current_size[5]
        if width < min_x:
            min_x = width
        if width > max_x:
            max_x = width
        if height < min_y:
            min_y = height
        if height > max_y:
            max_y = height
        if depth < min_z:
            min_z = depth
        if depth > max_z:
            max_z = depth

        return [min_x, max_x, min_y, max_y, min_z, max_z]

    @staticmethod
    def size_graph_predict(current_size, new_data):
        """
        Make sure that we can see the new dot on graph
        :param current_size:
        :param new_data:
        :return:
        """
        return [
            min(current_size[0], new_data[0]) - 100,
            max(current_size[1], new_data[0]) + 100,
            min(current_size[2], new_data[1]) - 100,
            max(current_size[3], new_data[1]) + 100,
        ]

    @staticmethod
    def round_hundred(x):
        """
        round graph height & width to next hundred
        :param x:
        :return:
        """
        return x if x % 100 == 0 else x + 100 - x % 100


    @staticmethod
    def train_test_side_by_side(var_train, target_train, var_test, target_test, features_list):
        """
        show training & testing model side by side
        :param features_list:
        :param var_train:
        :param target_train:
        :param var_test:
        :param target_test:
        :return:
        """
        fig, ax = plt.subplots(ncols=2, figsize=(10, 5))

        for var, target in zip(var_train, target_train):
            # current_size = Graph.size_training_3d_graph(current_size, var[0], var[1], var[2])
            ax[0].scatter(var[0], var[1], color=Graph.colors.get(target[0], 'black'), label=target[0], zorder=1)
        for var, target in zip(var_test, target_test):
            # current_size = Graph.size_training_3d_graph(current_size, var[0], var[1], var[2])
            ax[1].scatter(var[0], var[1], color=Graph.colors.get(target[0], 'black'), label=target[0], zorder=1)

        fig.suptitle("Training & validation graph", fontsize=16)

        # fig.tight_layout()
        # plt.subplots_adjust(left=.1, right=.1)
        # plt.subplot_tool()
        # plt.subplots_adjust(left=.5)
        ax[0].set_title(f"Training with {len(var_train)} sample", fontsize=10)
        ax[0].set_xlabel(features_list[0])
        ax[0].set_ylabel(features_list[1])
        ax[1].set_title(f"Test with {len(var_test)} sample", fontsize=10)
        ax[1].set_xlabel(features_list[0])
        ax[1].set_ylabel(features_list[1])
        # TODO plt.show()
        plt.savefig("train_test_side_by_side.png")
        plt.close()


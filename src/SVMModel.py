import matplotlib.pyplot as plt

from src import AIModel, Graph
from sklearn.svm import SVC


class SVMModel(AIModel):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.classifier = SVC()

    def train(self):
        """
        split data (x_train, y_train, x_test, y_test)
        train model with classifier.fit(x_train, y_train)
        display 2d or 3d graph depending on features number (x)
        :return:
        """
        self.split_data()
        self.classifier.fit(self.var_train, self.target_train.reshape(len(self.target_train, )))
        score = self.classifier.score(self.var_test, self.target_test)
        print(f'-----TRAINING RESULTS for {self.file if self.file else self.data}-------')
        print(
            f'nb_sample: {len(self.variable)} | {len(self.target)}, train : {len(self.var_train)} samples, test : {len(self.var_test)} samples')
        print(f'Score de {score}, {"Success training" if score > .8 else "Failed training => need more/better data"}')
        print(f'{self.precision()}')
        print(f'{self.confusion_matrix()}')

        self.generate_graaph()
        return self.classifier



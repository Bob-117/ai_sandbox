import logging
from datetime import datetime

from src import SVMModel

if __name__ == "__main__":
    logging.basicConfig(level='INFO', format='%(levelname)s AI_Sandbox %(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info(f"STARTING AI SANDBOX at {datetime.now()}")

    # TODO add malicious file for failed training example
    model_file_22 = SVMModel(file='src/data/data22.csv')
    model_file_22.train()

    new_data = [17, 117]
    model_file_22.run(new_data)

    new_data = [17, -117]
    model_file_22.run(new_data)

    new_data = [0, -0]
    model_file_22.run(new_data)

    new_data = [4, 50]
    model_file_22.run(new_data)

    model_file_33 = SVMModel(file='src/data/data33.csv')
    model_file_33.train()
    model_file_33.run([12, 117, 24])

    model_datasets = SVMModel(data='iris')
    model_datasets.train()
    # new_flower = ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)']
    new_flower = [1, 1, 7, 0]
    model_datasets.run(new_flower)
from reinforcment.src import GridEnv


def process():
    _obstacle_pos = [(2, 0)]
    _target_pos = [(1, 0)]
    env = GridEnv(
        _x=2, _y=2,
        obstacle_pos=_obstacle_pos,
        target_pos=_target_pos)
    env.render()

    env.policy_evaluation()
    env.policy_optimal()


if __name__ == '__main__':
    process()

import random
from typing import Any, SupportsFloat

import gymnasium as gym

import numpy as np
from gymnasium.core import RenderFrame, ObsType, ActType

import pprint


class GridEnv(gym.Env):

    def __init__(self, _x, _y, obstacle_pos, target_pos) -> None:
        self.size = (_x, _y)
        self.obstacle_pos = obstacle_pos
        self.target_pos = target_pos

        self.grid = np.zeros((_x, _y), dtype=np.int8)

        self.actions = {
            'L': (-1, 0),
            'R': (1, 0),
            'U': (0, -1),
            'D': (0, 1)
        }

        self.set_context()
        self.P = self.state_action_matrix()

        super().__init__()

    def step(self, action: ActType) -> tuple[ObsType, SupportsFloat, bool, bool, dict[str, Any]]:
        pass

    def reset(
            self, *, seed: int | None = None, options: dict[str, Any] | None = None
    ) -> tuple[ObsType, dict[str, Any]]:
        return super().reset(seed=seed, options=options)

    def render(self) -> RenderFrame | list[RenderFrame] | None:
        print(self.grid)
        return

    def close(self):
        super().close()

    def set_context(self, target_value=10, obstacle_value=-100):
        target_coord, obstacle_coord = tuple(zip(*self.target_pos)), tuple(zip(*self.obstacle_pos))

        # assert all(x < self.size for t in target_coord for x in t) works for a square grid
        try:
            if len(target_coord) > 0:
                self.grid[target_coord] = target_value
        except IndexError:
            print('cant set targets')

        try:
            if len(obstacle_coord) > 0:
                self.grid[obstacle_coord] = obstacle_value
        except IndexError:
            print('cant set obstacle')

    def state_action_matrix(self):
        P = {}

        for _x in range(self.size[0]):
            for _y in range(self.size[1]):
                # current_state = {(_x, _y): {}}
                available_actions = []

                for action_name, action_value in self.actions.items():
                    if self.allowed_action(_x, _y, action_value):
                        available_actions.append((action_name, action_value))
                available_s_prime = []

                for valid_action_name, valid_action in available_actions:
                    # s' next state
                    s_prime = self.get_new_state(_x, _y, valid_action)

                    # set reward
                    reward = self.grid[s_prime[0]][s_prime[1]]

                    # TODO stop if succes or fail ?
                    done = (reward == 10 or reward == -100)

                    # (s') = ( p(s'|s, a), r(s, a, s'), done )
                    available_s_prime.append({valid_action_name: (1 / len(available_actions), s_prime, reward, done)})

                P[(_x, _y)] = available_s_prime
                # for valid_action_name, valid_action in available_actions:
                #     current_state[(_x, _y)].update({
                #         valid_action_name: [
                #             (
                #                 # 1 / len(available_actions),
                #                 self.get_new_state(_x, _y, valid_action),
                #                 self.grid[_x][_y],
                #                 False
                #             )
                #         ]
                #     })
                #
                # P.update(current_state)
        # pprint.pprint(P, indent=4)
        return P

    def allowed_action(self, init_x, init_y, current_action):
        tmp_pos = tuple(map(lambda i, j: i + j, (init_x, init_y), current_action))
        allowed = 0 <= tmp_pos[0] < self.size[0] and 0 <= tmp_pos[1] < self.size[1]
        if not allowed:
            pass
            # print(f'cant move {current_action} from {init_x} - {init_y}')
        return allowed

    @staticmethod
    def get_new_state(init_x, init_y, current_action):
        return tuple(map(lambda i, j: i + j, (init_x, init_y), current_action))

    def policy_evaluation(self):
        pi = np.zeros([self.size[0] * self.size[1], len(self.actions)])

        # for each state and associated actions
        for s, actions in self.P.items():
            # for each action
            for action in actions:
                for action_key, _ in action.items():
                    # fuck this
                    move_index = {
                        'L': 0,
                        'R': 1,
                        'U': 2,
                        'D': 3
                    }
                    # print([key for key, value in self.actions.items() if value == action_key])
                    # print(s, move_index[action_key])
                    oof = move_index[action_key]
                    # print(action[action_key][0]) # => extract weight from {'L': (0.5, (1, 3), 10, True)}
                    weight = action[action_key][0]

                    pi[s[1] + s[0]][oof] = weight
        pprint.pprint(self.P, indent=8)
        print(pi)
        # V = np.zeros(self.size[0]*self.size[1])
        # print(V)

    def policy_optimal(self):
        env_data = self.P
        # Initialisation de la fonction de valeur
        V = np.zeros((self.size[0], self.size[1]))

        # Paramètres d'apprentissage
        discount_factor = 1.0  # facteur de réduction
        theta = 10  # seuil de convergence

        # Boucle principale de l'algorithme de programmation dynamique
        while True:
            delta = 0
            for state in env_data:
                v = V[state]
                new_v = 0
                for action in env_data[state]:
                    prob, next_state, reward, done = action[list(action.keys())[0]]
                    new_v += prob * (reward + discount_factor * V[next_state])
                V[state] = new_v
                delta = max(delta, abs(v - V[state]))
            if delta < theta:
                break

        # Affichage de la fonction de valeur
        print("Fonction de valeur:")
        print(V)

        # Calcul de la politique optimale
        policy = {}
        for state in env_data:
            best_action = None
            best_value = float('-inf')
            for action in env_data[state]:
                prob, next_state, reward, done = action[list(action.keys())[0]]
                value = reward + discount_factor * V[next_state]
                if value > best_value:
                    best_value = value
                    best_action = list(action.keys())[0]
            policy[state] = best_action

        # Affichage de la politique optimale
        print("Politique optimale:")
        print(policy)



def epsilon_greedy(q_values, epsilon):
    # on prend soit le meilleur choix, soit un random
    best_action = max(q_values, key=q_values.get) if random.random() > epsilon else random.choice(list(q_values.keys()))
    return best_action




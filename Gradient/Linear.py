from functools import partial

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation as anim


class Linear:

    def __init__(self):
        # Anscomb Quartet
        self.x = [10, 8, 13, 9, 11, 14, 6, 4, 12, 7, 5]
        self.y1 = [8.04, 6.95, 7.58, 8.81, 8.33, 9.96, 7.24, 4.26, 10.84, 4.82, 5.68]
        self.y2 = [9.14, 8.14, 8.74, 8.77, 9.26, 8.10, 6.13, 3.10, 9.13, 7.26, 4.74]
        self.y3 = [7.46, 6.77, 12.74, 7.11, 7.81, 8.84, 6.08, 5.39, 8.15, 6.42, 5.73]
        self.x4 = [8, 8, 8, 8, 8, 8, 8, 19, 8, 8, 8]
        self.y4 = [6.58, 5.76, 7.71, 8.84, 8.47, 7.04, 5.25, 12.5, 5.56, 7.91, 6.89]

        self.datasets = {
            'I': (self.x, self.y1),
            'II': (self.x, self.y2),
            'III': (self.x, self.y3),
            'IV': (self.x4, self.y4),
        }

        self.cost = 1

    def draw_data(self):
        """
        Show the Anscomb Quartet Dataset with matplotlib
        :return:
        """
        fig, axs = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(6, 6),
                                gridspec_kw={'wspace': .08, 'hspace': .08})

        axs[0, 0].set(xlim=(0, 20), ylim=(2, 14))
        axs[0, 0].set(xticks=(0, 10, 20), yticks=(4, 8, 12))

        for ax, (label, (x, y)) in zip(axs.flat, self.datasets.items()):
            ax.text(.1, .9, label, fontsize=20, transform=ax.transAxes, va='top')
            ax.tick_params(direction='in', top=True, right=True)
            ax.plot(x, y, 'o')

            ax.plot()
        plt.suptitle('Data visualisation')
        return fig, axs

    def regression(self):
        """
        Numpy linear regression on Anscomb Quartet Dataset
        :return:
        """
        fig, axs = self.draw_data()
        for ax, (label, (x, y)) in zip(axs.flat, self.datasets.items()):
            p1, p0 = np.polyfit(x, y, deg=1)
            ax.axline(xy1=(0, p0), slope=p1, color='r', lw=2)
            stats = (
                f'$\\mu$ = {np.mean(y):.2f}\n'
                f'$\\sigma$ = {np.std(y):.2f}\n'
                f'$r$ = {np.corrcoef(x, y)[0][1]:.2f}'
            )
            box = dict(boxstyle='round', fc='lightblue', ec='darkblue')
            ax.text(.95, .07, stats, fontsize=9, bbox=box, transform=ax.transAxes, horizontalalignment='right')

        plt.suptitle('Why do we always Linear regression limits (with numpy)')
        plt.show()
        return fig, axs

    def animate(self, i, line, x, data):
        # TODO LinearRegression.compute_cost() LinearRegression.run_linear_regression()
        self.cost = i
        while i < 10:  # last_cost > current_cost < new_cost
            line.set_ydata(x + i)
            break
        return line,

    def custom_regression(self):
        """
        Custom linear regression on Anscomb Quartet Dataset using cost function & derivative_theta
        :return:
        """
        fig, axs = self.draw_data()
        animations = []

        for ax, (label, (x, y)) in zip(axs.flat, self.datasets.items()):
            # ax.scatter(10, 10, c='red')
            # a = np.arange(0, 2 * np.pi, .01)
            # ax.plot(a, np.sin(a), c='yellow')
            # ani = anim.FuncAnimation(fig, self.animate, interval=20, blit=True, save_count=50)
            # ani.save('ok')



            a = np.arange(0, 2 * np.pi, .01)
            # print(a)
            regression, = ax.plot(a, a, c='red')
            animated_regression = anim.FuncAnimation(
                fig, partial(self.animate, line=regression, x=a, data=(x, y)), blit=True, save_count=50)
            animations.append(animated_regression)
            print(f'run : {self.cost}')
            stats = (
                f'Cost = {self.cost}-{np.pi:.6f}\n'
            )
            box = dict(boxstyle='round', fc='lightblue', ec='darkblue')
            ax.text(.95, .07, stats, fontsize=9, bbox=box, transform=ax.transAxes, horizontalalignment='right')
        plt.suptitle('Custom linear regression (with cost function J()')
        plt.show()
        return fig, axs, animations


if __name__ == '__main__':
    linear = Linear()

    linear.draw_data()
    plt.show()
    linear.regression()
    linear.custom_regression()

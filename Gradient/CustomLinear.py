from functools import partial

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation as anim


class CustomLinear:

    def __init__(self):
        self.variables = None
        self.target = None
        self.cost = "cost"
        self.coeff = [0, 0]  # y = alpha * x + beta

    def draw_data(self):
        """
        draw dataset with matplotlib
        :return:
        """

        self.variables = [10, 8, 13, 9, 11, 14, 6, 4, 12, 7, 5]
        self.target = [8.04, 6.95, 7.58, 8.81, 8.33, 9.96, 7.24, 4.26, 10.84, 4.82, 5.68]

        fig, ax = plt.subplots()

        ax.set(xlim=(-10, 20), ylim=(-10, 14))
        ax.set(xticks=(0, 10, 20), yticks=(4, 8, 12))
        # L = plt.legend(loc=1)
        for var, target in zip(self.variables, self.target):
            ax.tick_params(direction='in', top=True, right=True)
            ax.plot(var, target, 'o', color='darkblue')
        plt.suptitle('Data visualisation')
        return fig, ax

    def animate(self, i, line, x):
        # TODO LinearRegression.compute_cost() LinearRegression.run_linear_regression()
        while i < 100:  # last_cost > current_cost < new_cost (on chercher le zero du gradient ?)
            self.update_coeff(0.01)
            alpha, beta = self.coeff
            y = alpha * np.array(x) + beta
            # print(f'YYYY : {y}')
            # print(alpha.shape)
            # print(beta.shape)
            # print(np.array(x).shape)
            # print(y.shape)
            line.set_ydata(y)
            # L.set_tetx(i)
            break
        return line,

    def custom_regression(self):
        """
        Custom linear regression on Anscomb Quartet Dataset using cost function & derivative_theta
        :return:
        """
        fig, axs = self.draw_data()

        alpha, beta = self.coeff

        regression, = axs.plot(self.variables, (alpha * np.array(self.variables) + beta), c='red')
        animated_regression = anim.FuncAnimation(
            fig, partial(self.animate, line=regression, x=self.variables), blit=True, save_count=50)
        # data=(self.variables, self.target)
        stats = (
            f'Cost = {self.cost}-{np.pi:.6f}\n'
            f'Coeff : {self.coeff}'
        )
        box = dict(boxstyle='round', fc='lightblue', ec='darkblue')
        axs.text(.95, .07, stats, fontsize=9, bbox=box, transform=axs.transAxes, horizontalalignment='right')
        plt.suptitle('Custom linear regression (with cost function J()')
        plt.show()
        return fig, axs

    def predict(self, X=None):
        Y_pred = np.array([])
        X = X or self.variables
        coeff = self.coeff
        for x in X:
            Y_pred = np.append(Y_pred, coeff[0] + (coeff[1] * x))
        # print(Y_pred)
        return Y_pred

    def update_coeff(self, learning_rate):
        Y_pred = self.predict()
        Y = self.target
        m = len(Y)
        self.coeff[0] = self.coeff[0] - (learning_rate * ((1 / m) * np.sum(Y_pred - Y)))
        self.coeff[1] = self.coeff[1] - (learning_rate * ((1 / m) * np.sum((Y_pred - Y) * self.variables)))


if __name__ == '__main__':
    linear = CustomLinear()
    linear.draw_data()
    plt.show()
    linear.custom_regression()

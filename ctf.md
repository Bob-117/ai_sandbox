# Apprentissage par renforcement

# Contexte

La finalité de ce projet est l'obtention d'une solution IA d'aide aux CTF (voire un outil de pentest ?). 
Ce projet est un prétexte pour faire du machine learning par renforcement, et de la cybersécurité.
Au dela des résultats, les compétences mobilisées et apprises, les démarches et reflexions et les connaissances générales du sujet sont les objectifs principaux de ce projet.

`C'est ambitieux, ca va etre long, peut etre jamais fini, mais ptn ca va être drole :^)`

# Principe

- Markow

```mermaid

flowchart LR
    S0((S0))
    S1((S1))
    S2((S2))
    
    S0 -- .5 --> S1
    S0 -- .5 --> S2

    S1 -- .8 --> S2
    S1 -- .2 --> S1

    S2 -- .7 --> S0
    S2 -- .3 --> S1

    style S0 fill:#75ffa3    
    style S1 fill:#75ffa3
    style S2 fill:#75ffa3

```

- Richard Bellman

```mermaid

flowchart LR
    S0((S0))
    S1((S1))
    S2((S2))

    A0S0(((a0)))
    A1S0(((a1)))
    A0S1(((a0)))
    A1S1(((a1)))
    A0S2(((a0)))
    A1S2(((a1)))
    
    S0 --> A0S0:::Action
    S0 --> A1S0

    S1 --> A0S1
    S1 --> A1S1

    S2 --> A0S2
    S2 --> A1S2

    A0S0 -- .3 --> S0
    A0S0 -- .7 --> S1

    A1S0 -- .8 --> S0
    A1S0 -- .7 --> S2    

    A0S1 -- .2 --> S1
    A0S1 -- .8 --> S2

    A1S1 -- .1 --> S0 
    A1S1 -- .9 --> S2

    A0S2 -- .5 --> S1
    A0S2 -- .5 --> S2

    A1S2 -- .5 --> S0
    A1S2 -- .5 --> S2

    style S0 fill:#75ffa3    
    style S1 fill:#75ffa3
    style S2 fill:#75ffa3
    
    style A0S0 fill:#758aff
    style A1S0 fill:#758aff
    style A0S1 fill:#758aff
    style A1S1 fill:#758aff
    style A0S2 fill:#758aff
    style A1S2 fill:#758aff
```

### Elements clés du Machine learning par renforcement :

- [x] Un contexte
- [x] Un agent
- [x] Des états
- [x] Des actions (Explorer | Exploiter)
- [x] Un observateur (rewards)


# Implémentation


## User Story

- But :

Je donne à mon modele l'url d'un CTF, elle trouve comment s'inscrire, se connecter, trouver la liste des challenges, en selectionner 1, l'evaluer, proposer des solutions, mettre en place ces solutions.

- Cas concrets :

| #   | Intitulé                          | Solution proposée          | Exécution                         | Résolution           |
|-----|-----------------------------------|----------------------------|-----------------------------------|----------------------|
| 1   | reconnaitre un challenge web      | dirb                       | reconnaitre url/flag.txt          | envoyer FLAG{gibson} |
| 2   | reconnaitre la page d'inscription | reconnaitre un h1 register | remplir les champs                | se connecter         |
| 3   | reconnaitre un challenge reverse  | gihdra                     | reconnaitre un pattern assembleur | envoyer FLAG{gibson} |

*Entrainer des modèles plus spécifiques qu'un unique généraliste semble mieux pour commencer*

## Démarche

### Environnement 

Une plateforme CTF.

Commencer par un POC très simple (flask, nextjs, 2 chall).

<details>
<summary>Flask</summary>

```python

from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/login', methods=['POST'])
def login():
    username = request.json.get('username')
    password = request.json.get('password')

    # Check

    response = {'message': 'Success conn'}
    return jsonify(response), 200

@app.route('/register', methods=['POST'])
def register():
    username = request.json.get('username')
    password = request.json.get('password')
    
    # Check

    response = {'message': 'Success register'}
    return jsonify(response), 200

@app.route('/challenges', methods=['GET'])
def challenges():
    challenges_list = [
        {'id': 1, 'name': 'Challenge Web'},
        {'id': 2, 'name': 'Challenge Algo'},
        {'id': 3, 'name': 'Challenge Reverse'},
        # ...
    ]

    return jsonify(challenges_list), 200

@app.route('/challenges/web', methods=['POST'])
def solve_web_challenge():
    # Check

    response = {'message': 'Web solved'}
    return jsonify(response), 200

@app.route('/challenges/algo', methods=['POST'])
def solve_algo_challenge():
    # Check

    response = {'message': 'Algo solved'}
    return jsonify(response), 200

@app.route('/challenges/reverse', methods=['POST'])
def solve_reverse_challenge():
    # Check

    response = {'message': 'Reverse solved'}
    return jsonify(response), 200

if __name__ == '__main__':
    app.run(debug=True)


```

</details>


<details>
<summary>Nextjs</summary>

- https://youtu.be/MX11pscF8nc

</details>

### Processus d'apprentissage

Tout coder à la main pour comprendre. Choisir l'algorithme / comparer les algorithmes.

Liste des outils à disposition de notre agent

*=> matrice des états/actions oof*

Gérer les intéractions avec l'environnement :

- Avec la vraie page web ?

- Avec des dict json ? (abstraction du réel)

```json
{
    "case1": [
        "known_data": {
            "url": "index.html",
            "time": 0,
            "looking_for": "register"
        },
        "available_action": {
            "known_actions": [],
            "exploration": []
        }
    ]
}

```

### Choix de l'algo

Q-Learning, Policy Gradient, Proximal Policy Optimization, SARSA


### Fonction de récompenses

Se faire detecter ça compte ?


# Observations et limites

### Limites :

- mes compétences web pour mettre en place mon d'environnement et comprendre les enjeux
- mes compétences CTF pour mettre en place des challenges ou exploiter l'existant
- mes compétences CTF de connaissances des outils, méthodes et techniques
- l'accès et le traitement des données (comment on récupère, on simule, on trie, on utilise.......... :^))
- Une politique optimale est liée à l'environnement d'apprentissage

### Observations :

- Pipeline de modeles : 

````mermaid

flowchart LR

subgraph BlackBox
    subgraph categorisation

        subgraph supervised_ML
            a["SVM KNN"]
            b["Naive Bayes, Random Forest"]
        end

        subgraph deep_learning
            d["text analysis + url + all ctf data"]
        end

    end

    subgraph resolution

        subgraph reinforcement_ML
            agent
            environment
            state/action
        end
    end

    subgraph execution
        
        subgraph script

            python

            bash
        end

        subgraph output
            human["user instruction"]
        end
    end
end
    subgraph exploitation

        subgraph hehe1[fit learning]

        end

        subgraph hehe2[use irl]

        end

    end


categorisation --> resolution --> execution 
resolution & execution --> exploitation

style BlackBox fill:#67D0FD 


style categorisation fill:#2D7CFD   
style resolution fill:#2D7CFD     
style execution fill:#2D7CFD  

style exploitation fill:#168E3F

style supervised_ML fill:#86C81B
style deep_learning fill:#86C81B   
style reinforcement_ML fill:#86C81B  
style script fill:#86C81B
style output fill:#86C81B  


````

# Backlog 

Politique, Politique Optimale 𝝿*

Fonction de valeur V*(s) = maxΣT(s, a, s') [R(s, a, s') + γ V*(s')]

Exploration & Exploitation, Epsilon-Greedy

Q(s,a) = R(s,a) + γ max[Q(s',a')]

Q(s,a) ← (1 - α) Q(s,a) + α [R(s,a) + γ max[Q(s',a')]]

*avec le α tx apprentissage, γ le taux de réduction (convergence)*

Q-Learning, Policy Gradient, Proximal Policy Optimization (PPO)

Q-Learning : off-policy, SARSA : on-policy

Scikit Criteria, Tensorflow Keras, Pytorch, Gymnasium, Github, OpenAI Gym, TensorFlow Agents, Stable Baselines

Un modèle de catégorisation (compréhension de texte ?) pour trouver le type de chall 

Risk/reward : 5 min ragequit? dimensions temporelle

matrice états/actions Challenge web :

- Vuln : SQLI, SSTI, CSRF, XXE, XSS, cookies, token, buffer overflow
- Tools : dirb, payloads
- Mitre Recon :
    Active Scanning (3) = Scanning IP Blocks, Vulnerability Scanning, Wordlist Scanning
    Gather Victim Host Information (4) = Hardware, Software, Firmware, Client Configurations
    Gather Victim Identity Information (3) = Credentials, Email Addresses, Employee Names
    Gather Victim Network Information (6)= Domain Properties, DNS, Network Trust Dependencies, Network Topology, IP Addresses, Network Security Appliances
    Gather Victim Org Information (4) = Determine Physical Locations, Business Relationships, Identify Business Tempo, Identify Roles
    Phishing for Information (3) = Spearphishing Service, Spearphishing Attachment, Spearphishing Link
    Search Closed Sources (2) = Threat Intel Vendors, Purchase Technical Data
    Search Open Technical Databases (5) = DNS/Passive DNS, WHOIS, Digital Certificates, CDNs, Scan Databases
    Search Open Websites/Domains (3) = Social Media, Search Engines, Code Repositories       

# Notes

Domain du ML => Algo pour apprendre a resoudre une problématique
Entrainement d'un Agent dans un Environnement => diffère du Supervisé/NonS car pas de données => Directement un Agent et un Env, données directement de l'environnement
=> Donc on peut avoir un env virtuel puis transposer au réel (dur, mais pas besoin d'un dataset reel quoi)
=> Aeronautique, jeux video, faire marcher un robot

algorithme de Bellman-Ford = Dijkstra en plus lent mais poids négatifs

ou plan
pi* la politique optimale
la reward (gain = sum(reward))

pi* la politique optimale, ie la meilleur association (S, a) => la meilleure S'

si trop d'exploitation, manque des opportunités
R(s, a, s') reward entre s et s' en faisant a
T(s, a, s') probabilité d'une transition
on remarque un gamma
c'est en fait une matrice, qui compute les reawrd, avec un gamma pour qd meme converger

echantillonne l'env comme les méthodes de montecarlo (greedy epsilon par exemple)

Qlearning
DeepMind, AlphaGO => jeu de GO
avec le α tx apprentissage
γ le taux de réduction
off policy = pi est update a chaque tour

SARSA 
State-Action-Reward-State-Action
on policy : environnement changeant

nmap, masscan, dirb, dnsrecon, pwntools, ghidra, mitre attack mtx

https://exploit-notes.hdks.org/

# Sanbox IA

## *Entrainement et classification à partir des données connues par apprentissage supervisé*

```shell

git clone git@gitlab.com:Bob-117/ai_sandbox.git
cd ai_sandbox
git checkout develop
pip install -r requirements.txt

make run

```

[[_TOC_]]

# Principe

```mermaid

graph LR

Variables[Variables explicatives <br> x1, x2...xn] --> AI{"F(x1, x2...xn | p1, p2...pm)"}
Parameters[Hyperparamètres <br> p1, p2...pm] --> AI
AI --> Output[Variables à expliquer / target <br> y]
```

Notre premiere approche sera un apprentissage supervisé avec des données connues (*labeled data*).
Ces données seront séparées aléatoirement en deux jeux de données distincts,
un jeu de données pour l'entrainement et un jeu de données pour tester notre modele et valider notre entrainement.
![train_test_side_by_side](/Gradient/img/train_test_side_by_side.png "train_test_side_by_side").


<details>
<summary> data.csv</summary>
<table>
    <tr><td>metadata1</td><td>metadata2</td><td>class</td></tr>
    <tr><td>-7</td><td>14</td><td>0</td></tr>
    <tr><td>-72</td><td>52</td><td>0</td></tr>
    <tr><td>-63</td><td>53</td><td>0</td></tr>
    <tr><td>...</td><td>...</td><td>...</td></tr>
    <tr><td>68</td><td>76</td><td>1</td></tr>
    <tr><td>14</td><td>94</td><td>1</td></tr>
    <tr><td>61</td><td>91</td><td>1</td>
    </tr>
</table>
</details>

<details>
<summary> Architecture de code (lib scikit learning)</summary>

```python
import pandas as pd

from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn import model  # adapt

import pickle

from numpy import array

# x = variables, y = target

OUTPUT_VALUES = {
    0: 'Sain',
    1: 'Malware'
}
# data gathering
data = pd.read_csv('data.csv')
(entry_data, entry_class) = (data.iloc[:, :-1].values, data.iloc[:, -1].values)

# Labeled data handling
x_train, x_test, y_train, y_test = train_test_split(entry_data, entry_class, test_size=.2)
# Training
model.fit(x_train, y_train)
# validation : confusion matrice
confusion = confusion_matrix(y_test, model.predict(x_test))
print(confusion)
# validation : score
model.score(x_train, y_train)
# save
pickle.dump(model, open('model_pickle', 'wb'))
# reload & run 
model = pickle.load(open('scv_model_pickle', 'rb'))
input_data = [17, 18]
y_pred = model.predict(
    array([input_data])
)
print(OUTPUT_VALUES[y_pred[0]])  # ie print 'Malware' if y_pred == 1 else 'Sain'
```

</details>

L'entrainement et l'utilisation de notre modele (quelqu'il soit) seront guidés par les étapes comme suit :

- Collecte de données connues (labeled data)
- Creation de nos matrices de données (X = [nb_sample, nb_feature] et Y = [nb_sample, nb_target])
    - *nb_feature le nombre de paramètres qualifiants*
- Visualisation avec Matplotlib
- Entrainement de notre model (*model.fit()*)
- Validation de notre model (*model.score()*, matrice de confusion avec le jeu de données de test préalablement isolé)
- Utilisation de notre modele sur une nouvelle donnée entrante *y_predict = model.predict(new_data)*

# Process

- [X] Creation d'un dataset aléatoire
- [X] Altération
- [X] Traitement (NaN, outliers, normalisation)
- [X] Visualisation
- [X] Entrainement Decision Tree
- [X] Entrainement Random Forest
- [X] Comparaison ROC SVM KNN

<i> Attention à la création des fichiers `data.csv `, `final.csv` et des différentes images matplotlib sur votre machine :^)</i>

```sh
cd process | python __main__.py
```

![](process/all_2d.png)

<i>Visualisation du dataset</i>

![](process/bar.png)
![](process/density.png)

<i>Normalisation entre 0 et 1.</i>

![](process/decision_tree_estimator.png)

<i>Decision tree estimator</i>

![](process/random_forest_estimator.png)

<i>Random forest importances</i>

![](process/ROC_KNN_SVM.png)

<i>Evaluation ROC</i>

# Gradient

Quelques scripts ML / data à trier ig.

# Dropbox

## Points clés

- [ ] Modeliser
- [ ] Developper
- [ ] Securiser
- [ ] Tester
- [ ] Analyser
- [ ] Documenter
- [ ] Deployer

## Securité

- [ ] Malicious input (fooling the system)
- [ ] Data poisoning
- [ ] Online system is still learning
- [ ] Popular model are easy to attack
- [ ] Data privacy / confidentiality

## Solutions

Les modeles (scikit learning):

- Classification / Regression
- Reconnaissance de motifs, systemes de recommandation, substitution de valeurs manquantes
    - [ ] SVM
        - Representation en N dimensions (nos N variables explicatives) de nos données d'entrainement
        - Recherche de l'hyperplan separateur optimal entre nos différents nuages de points
        - Capable de trouver des motifs non-linéaires grace a une hypersurface qui maximise la marge entre classes
        - Choix du kernel (regression lineaire, sigmoid...)
        - Pour une nouvelle entrée, le nouveau point est comparé aux nuages connus
    - [ ] KNN
        - Representation en N dimensions (nos N variables explicatives) de nos données d'entrainement
        - Pour une nouvelle entrée, analyse des k plus proches voisins et caracterisation
        - Attention à la pertinence et a la precision qui depend de k le nombre de voisins
- A etudier :
    - [ ] *random forest*
    - [ ] *Naive Bayes (text data)*

Les lib :

- [ ] scikit-learning
- [ ] numpy
- [ ] matplotlib
- [ ] pandas
- [ ] pickle
- [ ] *pytorch*
- [ ] *edward*
- [ ] *pyspark*

## Backlog

https://towardsdatascience.com/a-technical-introduction-to-experience-replay-for-off-policy-deep-reinforcement-learning-9812bc920a96

```mermaid

graph TB
    subgraph Reinforcement
        A[Agent]
        E[Environment]
        O[Observer]
        P[Policy]
        S[State]
    end
    
    O -- Observes --> S
    O -- Provides observations and rewards --> E
    A -- interact --> E
    P -. Maps states to actions -.- S --> A
    O -- Reward --> A
    O <-- π* --> P
```

```mermaid

graph TB
    subgraph Notes
        V[Value Function]
        Q[Q-Values]
        Alg[Learning Algorithm]
        Markov
        Trade-off
        PG["Policy Gradient"]
        RB["Replay Buffer"]
    end
```
